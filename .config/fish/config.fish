if status is-interactive
    # Commands to run in interactive sessions can go here
	starship init fish | source
	zoxide init fish | source
	set -U fish_greeting ""

	# atualizar sistema full
	# alias emergeUpdate="doas emerge -uNDa @world"

	# atualizar de forma simples
	alias emergeUpdateSimple="doas emerge --sync && doas emerge-webrsync"

	# atualizar com modificação no make
	alias emergeMakeUpdate="doas emerge --update --quiet --deep --with-bdeps=y --newuse @world"

	alias v="nvim"
	alias dwm.c="cd ~/.config/dwm && v config.h"
	alias fish.fish="v ~/.config/fish/config.fish"
	# alias sourceFish="~/.config/fish/config.fish"
	alias st.c="cd ~/.config/dwm/st && v config.h"
	alias less="bat"
	alias tree="et"
	alias ls="natls"
	alias treeb="broot"
end
