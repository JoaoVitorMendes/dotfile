//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	{"", "~/.suckless/dwmblocks/scripts/memory",		5,		0},

	{"", "~/.suckless/dwmblocks/scripts/network",		5,		0},
	
	{"", "~/.suckless/dwmblocks/scripts/time",			5,		0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = "";
static unsigned int delimLen = 5;
