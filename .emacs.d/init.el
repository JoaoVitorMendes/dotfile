



;;;;  _/_/_/    _/_/_/      _/_/    _/_/_/  _/      _/  _/_/_/_/  _/    _/    _/_/_/  _/    _/
;;;; _/    _/  _/    _/  _/    _/    _/    _/_/    _/  _/        _/    _/  _/        _/  _/
;;;;_/_/_/    _/_/_/    _/_/_/_/    _/    _/  _/  _/  _/_/_/    _/    _/  _/        _/_/
;;;_/    _/  _/    _/  _/    _/    _/    _/    _/_/  _/        _/    _/  _/        _/  _/
;;_/_/_/    _/    _/  _/    _/  _/_/_/  _/      _/  _/          _/_/      _/_/_/  _/    _/
;;																	 ___ __ _  ___ ________
;;																	/ -_)  ' \/ _ `/ __(_-<
;;																	\__/_/_/_/\_,_/\__/___/



;;; init.el --- João Vitor emacs configuration -*- lexical-binding: t; -*-
;;; Author: Joao Vitor <joaovitorm@protonmail.ch>
;;; URL: https://gitlab.com/JoaoVitorMendes/dotfile/-/blob/main/.emacs.d/init.el

;;; Commentary:

;;; This config targets Emacs 30

;;; init.el --- João Vitor emacs configuration -*- lexical-binding: t; -*-

;;; Author: Joao Vitor <joaovitorm@protonmail.ch>
;;; URL: https://gitlab.com/JoaoVitorMendes/dotfile/-/blob/main/.emacs.d/init.el

;;; Commentary:

;;; This config targets Emacs 30

;;; forcer's .emacs -- emacs, this is -*- emacs-lisp -*-

;;; i don't use XEmacs.  This file does not work with XEmacs.
( when ( featurep 'xemacs )
  ( error "This .emacs file does not work with XEmacs." ))


;; local keybinds
(local-set-key "." 'electric-dot-and-dash-dot)
(local-set-key "-" 'electric-dot-and-dash-dash)

(modify-syntax-entry ?^ "($")
(modify-syntax-entry ?$ ")^")

(global-set-key (kbd "<escape>") 'keyboard-escape-quit)
(global-set-key (kbd "C-c d") 'insert-date) ;; 13.04.2004
(global-set-key (kbd "C-s-d") 'insert-date) ;; Brazil, 13. April 2004
(global-set-key (kbd "TAB") 'self-insert-command)

(global-set-key (kbd "C-+") 'toggle-hiding)
(global-set-key (kbd "C-\\") 'toggle-selective-display)

(global-set-key [down-mouse-2] 'mouse-flash-position)
(global-set-key [S-down-mouse-2] 'mouse-scan-lines-or-M-:)
(global-set-key [S-down-mouse-2] 'mouse-scan-lines)
(global-set-key [down-mouse-2]   'mouse-flash-position-or-M-x)
(global-set-key [?\C-c ?m] 'column-marker-1)

(global-set-key "\C-x\ \C-r" 'recentf-open-files)

(define-key emacs-lisp-mode-map (kbd "C-x M-t") 'counsel-load-theme)	 ;; ctrl x Alt t

;; Move Up to Down
(global-set-key (kbd "M-<up>") 'move-dup-move-lines-up)
(global-set-key (kbd "M-<down>") 'move-dup-move-lines-down)
(global-set-key (kbd "C-M-<up>") 'move-dup-duplicate-up)
(global-set-key (kbd "C-M-<down>") 'move-dup-duplicate-down)

(global-set-key (kbd "C-9") 'close-quoted-open-paren-right)
(global-set-key (kbd "C-)") 'close-all-open-paren-right)
(global-set-key (kbd "C-8") 'close-quoted-open-paren-left)
(global-set-key (kbd "C-(") 'close-all-open-paren-left)

;; Comment area KeyBinds
(global-set-key (kbd "s-/") 'comment-line)
(global-set-key (kbd "M-s-/") 'comment-box)


;; undo and redo
(global-unset-key (kbd "C-z"))
(global-set-key (kbd "C-z")   'undo-fu-only-undo)
(global-set-key (kbd "C-s-u") 'undo-fu-only-redo)

;; Display line numbers in every buffer
(global-display-line-numbers-mode -1)

;;; The default "C-x c" is quite close to "C-x C-c", which quits Emacs.
;;; Changed to "C-c h". Note: We must set "C-c h" globally, because we
;;; cannot change `helm-command-prefix-key' once `helm-config' is loaded.

(global-set-key (kbd "C-c h") 'helm-command-prefix)
(global-set-key (kbd "M-x") 'helm-M-x)
(global-set-key (kbd "C-M-j") 'helm-buffers-list)  ;; Ctrl Alt J
(global-unset-key (kbd "C-x c"))
(global-set-key (kbd "C-x C-f") 'helm-find-files)
(global-set-key (kbd "C-x C-d") 'helm-browse-project)
(global-set-key (kbd "C-x r p") 'helm-projects-history)
(global-set-key (kbd "C-M-s") 'helm-multi-swoop-all)

;; for vanilla emacs
(global-set-key (kbd "C-c p") 'parrot-rotate-prev-word-at-point)
(global-set-key (kbd "C-c n") 'parrot-rotate-next-word-at-point)

(define-key global-map (kbd "RET") 'newline-and-indent)

;;; Initialize package sources
;;; Install straight.el

( defvar bootstrap-version )
( let (( bootstrap-file
         ( expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory ))
       ( bootstrap-version 6 ))
  ( unless (file-exists-p bootstrap-file )
    ( with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies )
      ( goto-char (point-max ))
      ( eval-print-last-sexp )))
  ( load bootstrap-file nil 'nomessage ))


;;; Disable package.el in favor of straight.el
( setq package-enable-at-startup nil )

;;; Straight packages

;;; Use straight.el for use-package expressions
( straight-use-package 'use-package )

;;; Theme

( straight-use-package
 '(
   stimmung-themes :host github :repo "motform/stimmung-themes"
   :branch "master" ))

;;( load-theme 'stimmung-themes-dark t )


( straight-use-package
 '(
   autothemer :host github :repo "jasonm23/autothemer"
   :branch "master" ))

( straight-use-package
 '(
   emacs-chocolate-theme :host github :repo "SavchenkoValeriy/emacs-chocolate-theme"
   :branch "master" ))
( load-theme 'chocolate t )

( straight-use-package 'ivy )
( straight-use-package 'all-the-icons-ivy )
( straight-use-package 'counsel )
( straight-use-package 'rainbow-delimiters )
( straight-use-package 'which-key )
( straight-use-package 'hydra )
( straight-use-package 'projectile )
( straight-use-package 'counsel-projectile )
( straight-use-package 'magit )
( straight-use-package 'git-gutter )
( straight-use-package 'fill-column-indicator )
( straight-use-package 'forge )


( straight-use-package 'projectile )
( straight-use-package 'flycheck )
( straight-use-package 'company-flx )
( straight-use-package 'company )
( straight-use-package 'company-box )
( straight-use-package 'company-statistics )
( straight-use-package 'company-lsp )


( straight-use-package
  '(
    format-all :host github :repo "emacsmirror/format-all"
    :branch "master" ))


( straight-use-package
  '(
    lsp-java :host github :repo "emacs-lsp/lsp-java"
    :branch "master" ))

( straight-use-package
  '(
    consult-lsp :host github :repo "gagbo/consult-lsp"
    :branch "main" ))

( straight-use-package
  '(
    doom-modeline :host github :repo "seagle0128/doom-modeline"
    	:branch "master" ))

( straight-use-package 'lsp-mode )
( straight-use-package 'lsp-ui )

( straight-use-package
  '( lsp-dart :host github :repo "emacs-lsp/lsp-dart"
     :branch "master" ))


( straight-use-package
  '( hover.el :host github :repo "ericdallo/hover.el"
     :branch "master" ))


( straight-use-package
  '( emacs-dashboard :host github :repo "emacs-dashboard/emacs-dashboard"
     :branch "master" ))

( straight-use-package 'dimmer )

( straight-use-package
  '( literate-calc-mode.el :host github :repo "sulami/literate-calc-mode.el"
     :branch "master" ))

( straight-use-package 'rainbow-mode )
( straight-use-package 'highlight-thing )
( straight-use-package 'mode-icons )
( straight-use-package 'move-dup )
( straight-use-package 'undo-fu )
( straight-use-package 'fancy-compilation )

( straight-use-package 'highlight-defined )
( straight-use-package 'nyan-mode )
( straight-use-package 'parrot )
( straight-use-package 'eldoc-box )
( straight-use-package 'c-eldoc )
( straight-use-package 'yasnippet )
( straight-use-package 'highlight-indent-guides )
( straight-use-package 'pretty-mode )

( straight-use-package 'volatile-highlights )
( straight-use-package 'paren )
( straight-use-package 'highlight-parentheses )
( straight-use-package 'idle-highlight-mode )
( straight-use-package 'column-enforce-mode )
( straight-use-package 'paredit )
( straight-use-package 'ligature )
( straight-use-package 'auto-highlight-symbol )
( straight-use-package 'highlight-numbers )
( straight-use-package 'origami )
( straight-use-package 'real-auto-save )
( straight-use-package 'latex-pretty-symbols )
( straight-use-package 'flycheck-posframe )
( straight-use-package 'flyspell )
( straight-use-package 'flyspell-correct )
( straight-use-package 'dap-mode )

( straight-use-package
  '( color-identifiers-mode :host github :repo "ankurdave/color-identifiers-mode"
     :branch "master" ))


( straight-use-package 'treemacs )
( straight-use-package 'lsp-treemacs )

( straight-use-package
  '( flycheck-pkg-config :host github :repo "Wilfred/flycheck-pkg-config"
     :branch "master" ))

( straight-use-package
  '( flycheck-package :host github :repo "purcell/flycheck-package"
     :branch "master" ))


( straight-use-package
  '( lsp-dart :host github :repo "emacs-lsp/lsp-dart"
     :branch "master" ))

( straight-use-package
  '( flycheck-haskell :host github :repo "flycheck/flycheck-haskell"
     :branch "master" ))

( straight-use-package
  '( flycheck-prospector :host github :repo "chocoelho/flycheck-prospector"
     :branch "master" ))

;;; pip install prospector and use prospector

( straight-use-package
  '( flycheck-rust :host github :repo "flycheck/flycheck-rust"
     :branch "master" ))

( straight-use-package
  '( flycheck-checkbashisms :host github :repo "cuonglm/flycheck-checkbashisms"
     :branch "main" ))

( straight-use-package
  '( gcmh  :host github :repo "emacsmirror/gcmh"
     :branch "master" ))

( straight-use-package
  '( Smartparens :host github :repo "Fuco1/smartparens"
     :branch "master" ))

( straight-use-package
  '( hideshowvis :host github :repo "sheijk/hideshowvis"
     :branch "master" ))

( straight-use-package
  '( helm :host github :repo "emacs-helm/helm"
     :branch "master" ))

( straight-use-package
  '( helm-ls-git :host github :repo "emacs-helm/helm-ls-git"
     :branch "master" ))

( straight-use-package
  '( helm-descbinds :host github :repo "emacs-helm/helm-descbinds"
     :branch "master" ))

( straight-use-package
  '( helm-lsp :host github :repo "emacs-lsp/helm-lsp"
     :branch "master" ))


( straight-use-package
  '( Emacs-wgrep :host github :repo "mhayashi1120/Emacs-wgrep"
     :branch "master" ))

( straight-use-package
  '( helm-swoop :host github :repo "emacsorphanage/helm-swoop"
     :branch "master" ))

( straight-use-package
  '( ellama :host github :repo "s-kostyaev/ellama"
     :branch "main" ))


( straight-use-package
  '( undo-tree :host gitlab :repo "tsc25/undo-tree"
     :branch "master" ))

( straight-use-package 'tree-sitter )
( straight-use-package 'tree-sitter-langs )

( straight-use-package 'all-the-icons )

(when (display-graphic-p)
  (require 'all-the-icons))

(use-package lambda-line
  :straight (:type git :host github :repo "lambda-emacs/lambda-line")
  :custom
  (lambda-line-icon-time t) ;; requires ClockFace font (see below)
  (lambda-line-clockface-update-fontset "ClockFaceRect") ;; set clock icon
  (lambda-line-position 'top)       ;; Set position of status-line
  (lambda-line-abbrev t)            ;; abbreviate major modes
  (lambda-line-hspace "  ")        ;; add some cushion
  (lambda-line-prefix t)            ;; use a prefix symbol
  (lambda-line-prefix-padding nil)    ;; no extra space for prefix
  (lambda-line-status-invert nil)     ;; no invert colors
  (lambda-line-gui-ro-symbol  " ") ;; symbols
  (lambda-line-gui-mod-symbol " ")
  (lambda-line-gui-rw-symbol  " ")
  (lambda-line-space-top +.10) ;; padding on top and bottom of line
  (lambda-line-space-bottom -.10)
  (lambda-line-symbol-position 0.1) ;; adjust the vertical placement of symbol
  :config
  ;; activate lambda-line
  ;; (lambda-line-mode)
  ;; set divider line in footer
  (when (eq lambda-line-position 'top)
    (setq-default mode-line-format (list "%_"))
    (setq mode-line-format (list "%_"))))

(use-package fontset
  :straight (:type built-in) ;; only include this if you use straight
  :config
  ;; Use symbola for proper unicode
  (when (member "Symbola" (font-family-list))
    (set-fontset-font
     t 'symbol "Symbola" nil)))

(customize-set-variable 'flymake-mode-line-counter-format '("" flymake-mode-line-error-counter flymake-mode-line-warning-counter flymake-mode-line-note-counter ""))
(customize-set-variable 'flymake-mode-line-format '(" " flymake-mode-line-exception flymake-mode-line-counters))





;;; Packages local

( add-to-list 'load-path "~/.emacs.d/plugins/code" )
( add-to-list 'load-path "~/.emacs.d/plugins/discord" )
( add-to-list 'load-path "~/.emacs.d/plugins/interface" )
( add-to-list 'load-path "~/.emacs.d/plugins/line" )

(setq custom-file "~/.emacs.d/custom-settings.el")
(load custom-file t) ;;; configuracao local


;;; Requires

( require 'ido )
( ido-mode t )
  ;;; C-x b

( require 'dimmer )
( dimmer-configure-which-key )
( dimmer-configure-helm )
( dimmer-mode t )

( require 'highlight-thing )
( global-highlight-thing-mode )

( require 'counsel )
( require 'ivy )
( require 'move-dup )
( require 'origami )
( require 'eldoc-box )
( require 'undo-tree )

( require 'tree-sitter )
( require 'tree-sitter-langs )

( require 'pretty-mode )
  ;;; if you want to set it globally
( global-pretty-mode t )
( pretty-activate-groups
  '( :sub-and-superscripts :greek :arithmetic-nary ))

( require 'real-auto-save )
( add-hook 'prog-mode-hook 'real-auto-save-mode )
( setq real-auto-save-interval 5 ) ;;; in seconds

( require 'highlight-parentheses )
( add-hook 'prog-mode-hook #'highlight-parentheses-mode )
( add-hook 'minibuffer-setup-hook #'highlight-parentheses-minibuffer-setup )

( require 'eldoc )

( require 'helm )
( require 'helm-ls-git )
(require 'helm-descbinds)
(require 'wgrep)
(require 'helm-swoop)

( require 'elcord )
( elcord-mode )

( load "cursor-chg.el" )
( require 'cursor-chg )

( load  "column-marker.el" )
( require 'column-marker )

( load "highlight-sexp.el" )
( require 'highlight-sexp )

( load "latex-pretty-symbols.el" )
( require 'latex-pretty-symbols )

( load "beacon.el" )
(require 'flycheck-checkbashisms)
(eval-after-load 'flycheck '(add-hook 'flycheck-mode-hook #'flycheck-checkbashisms-setup))


(require 'lsp-java)
( add-hook 'java-mode-hook #'lsp )
( require 'dap-java )
( require 'dap-cpptools )
( require 'dap-elixir )
( require 'dap-erlang )
( require 'dap-php )



;;; Settings


( use-package ivy
  :diminish
  :bind (
         :map ivy-minibuffer-map
         (  "TAB" . ivy-alt-done )
         ( "C-l" . ivy-alt-done )
         ( "C-j" . ivy-next-line )
         ( "C-k" . ivy-previous-line )
         :map ivy-switch-buffer-map
         ( "C-k" . ivy-previous-line )
         ( "C-l" . ivy-done)
         ( "C-d" . ivy-switch-buffer-kill )
         :map ivy-reverse-i-search-map
         ( "C-k" . ivy-previous-line )
         ( "C-d" . ivy-reverse-i-search-kill ))
  :config
  ( ivy-mode 1 ))

;; local keybinds
(local-set-key "." 'electric-dot-and-dash-dot)
(local-set-key "-" 'electric-dot-and-dash-dash)

(modify-syntax-entry ?^ "($")
(modify-syntax-entry ?$ ")^")


(global-set-key (kbd "<escape>") 'keyboard-escape-quit)
(global-set-key (kbd "C-c d") 'insert-date) ;; 13.04.2004
(global-set-key (kbd "C-s-d") 'insert-date) ;; Brazil, 13. April 2004
(global-set-key (kbd "TAB") 'self-insert-command)

(global-set-key (kbd "<f12>") #'pomidor)


(global-set-key (kbd "C-+") 'toggle-hiding)
(global-set-key (kbd "C-\\") 'toggle-selective-display)

(global-set-key [down-mouse-2] 'mouse-flash-position)
(global-set-key [S-down-mouse-2] 'mouse-scan-lines-or-M-:)
(global-set-key [S-down-mouse-2] 'mouse-scan-lines)
(global-set-key [down-mouse-2]   'mouse-flash-position-or-M-x)
(global-set-key [?\C-c ?m] 'column-marker-1)

(global-set-key "\C-x\ \C-r" 'recentf-open-files)

(define-key emacs-lisp-mode-map (kbd "C-x M-t") 'counsel-load-theme)	 ;; ctrl x Alt t

(global-set-key (kbd "C-c f") 'flyspell-toggle )

( use-package all-the-icons-ivy
  :init ( add-hook 'after-init-hook 'all-the-icons-ivy-setup ))
:config
( setq all-the-icons-ivy-file-commands
  '( counsel-find-file counsel-file-jump counsel-recentf counsel-projectile-find-file counsel-projectile-find-dir counsel-M-x ))


( use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode  ))
( add-hook 'c-mode-hook 'rainbow-delimiters-mode )

( use-package which-key
  :init ( which-key-mode )
  :diminish which-key-mode
  :config
  ( setq which-key-idle-delay 0.3 ))


( use-package hydra )

( defhydra hydra-text-scale ( :timeout 4 )
  "scale text"
  ( "j" text-scale-increase "in" )
  ( "k" text-scale-decrease "out" )
  ( "f" nil "finished" :exit t ))


( use-package projectile
  :diminish projectile-mode
  :config ( projectile-mode )
  :bind-keymap
  ( "C-c p" . projectile-command-map )
  :init
  (when (file-directory-p "~/Projects/Code" )
    ( setq projectile-project-search-path '("~/Projects/Code" )))
  ( setq projectile-switch-project-action #'projectile-dired ))

;;; Advanced project switching: C-c p p
;;; Quick searching with counsel-projectile-rg - C-c p s r
;;;  Results to buffer with C-c C-o

( use-package counsel-projectile
  :after projectile
  :config
  ( counsel-projectile-mode 1 ))

;;; Run magit-status in buffer from Git repo, press ? for command panel.
;;; Refresh buffer with g r

( use-package magit
  :commands (magit-status magit-get-current-branch )
  :custom
  ( magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1 ))

( use-package git-gutter
  :custom
  ( git-gutter:modified-sign "~" )
  ( git-gutter:added-sign    "+" )
  ( git-gutter:deleted-sign  "-" )
  :custom-face
  ( git-gutter:modified ((t (:background "#f1fa8c" ))))
  ( git-gutter:added    ((t (:background "#50fa7b" ))))
  ( git-gutter:deleted  ((t (:background "#ff79c6" ))))
  :config
  ( global-git-gutter-mode +1 ))

( use-package fill-column-indicator
  :hook
  (( markdown-mode
     git-commit-mode ) . fci-mode ))

;;; login with github
( use-package forge )



( dashboard-setup-startup-hook )
( setq initial-buffer-choice ( lambda ( ) ( get-buffer-create "*Fedora*" )))
( setq dashboard-items '( ))
    ;;; Set the title
( setq dashboard-startup-banner "~/.emacs.d/dashboard/dashboard.txt" )
( setq dashboard-center-content  nil )
( setq dashboard-banner-logo-title "" )
( setq dashboard-set-init-info nil )
( setq dashboard-show-shortcuts nil )
( setq dashboard-set-footer nil )


( use-package dimmer )

( unless ( package-installed-p 'crux )
  ( package-refresh-contents )
  ( package-install 'crux ))


( use-package rainbow-mode )
( rainbow-mode t )

( use-package highlight-thing )

( setq highlight-thing-delay-seconds 1.5 )
( setq highlight-thing-limit-to-defun t )
( setq highlight-thing-case-sensitive-p t )
  ;;; Don't highlight the thing at point itself. Default is nil.
( setq highlight-thing-exclude-thing-under-point nil )
  ;;; (setq highlight-thing-ignore-list '("False" "True" ))
( setq highlight-thing-all-visible-buffers-p t )
( setq highlight-thing-limit-to-region-in-large-buffers-p nil
  highlight-thing-narrow-region-lines 15
  highlight-thing-large-buffer-limit 5000 )

( use-package mode-icons )
( mode-icons-mode )

( use-package move-dup
  :bind (( "M-p"   . move-dup-move-lines-up )
         ( "C-M-p" . move-dup-duplicate-up )
         ( "M-n"   . move-dup-move-lines-down )
         ( "C-M-n" . move-dup-duplicate-down )))

( use-package undo-fu )
( setq undo-limit 67108864) ;;; 64mb.
( setq undo-strong-limit 100663296 ) ;;; 96mb.
( setq undo-outer-limit 1006632960 ) ;;; 960mb.

( use-package fancy-compilation
  :commands (fancy-compilation-mode ))

( with-eval-after-load 'compile
  ( fancy-compilation-mode ))

( use-package highlight-defined )
;;; (add-hook 'emacs-lisp-mode-hook 'highlight-defined-mode )
;;; Altere as seguintes faces:

    ;;; highlight-defined-function-name-face
    ;;; highlight-defined-builtin-function-name-face
    ;;; highlight-defined-special-form-name-face
    ;;; highlight-defined-macro-name-face
    ;;; highlight-defined-variable-name-face
    ;;; highlight-defined-face-name-face

( use-package nyan-mode )
( nyan-mode )
( nyan-start-animation )
( setq nyan-animate-nyancat t )
  ;;; (setq nyan-wavy-trail nil )



( use-package c-eldoc )
( setq c-eldoc-includes "`pkg-config gtk+-2.0 --cflags` -I./ -I../ " )
( add-hook 'c-mode-hook 'c-turn-on-eldoc-mode )
( add-hook 'c-mode-hook 'c-turn-on-eldoc-mode )
( add-hook 'c++-mode-hook 'c-turn-on-eldoc-mode )
( setq c-eldoc-buffer-regenerate-time 60 )
( defvar c-eldoc-cpp-command "/lib/cpp " ) ;;; compiler
( defvar c-eldoc-cpp-macro-arguments "-dD -w -P" )
( defvar c-eldoc-cpp-normal-arguments "-w -P" )
( defvar c-eldoc-includes "`pkg-config gtk+-2.0 --cflags` -I./ -I../ ") ;;; include flags
( setq c-eldoc-cpp-command "/usr/local/bin/clang" )


( use-package yasnippet )
( yas-reload-all )
( add-hook 'prog-mode-hook #'yas-minor-mode )


( use-package highlight-indent-guides
  :diminish
  :hook
  (( prog-mode yaml-mode ) . highlight-indent-guides-mode )
  :custom
  ( highlight-indent-guides-auto-enabled t )
  ( highlight-indent-guides-responsive t )
  ( highlight-indent-guides-method 'character )
  (set-face-background 'highlight-indent-guides-character "green")
  (set-face-background 'highlight-indent-guides-character-face "red")) ;;; column






( use-package pretty-mode )

( use-package volatile-highlights
  :diminish
  :hook
  ( after-init . volatile-highlights-mode )
  :custom-face
  ( vhl/default-face (( nil ( :foreground "#2B3C44" :background "#282828" )))))

( use-package paren
  :ensure nil
  :hook
  ( after-init . show-paren-mode )
  :custom-face
  ( show-paren-match (( nil ( :background "#261D1F" :foreground "#4BC88C" ))))
  :custom
  ( show-paren-style 'mixed )
  ( show-paren-when-point-inside-paren t )
  ( show-paren-when-point-in-periphery t ))

( use-package highlight-parentheses
  :ensure t )

( use-package idle-highlight-mode
  :config ( setq idle-highlight-idle-time 0.2 )

  :hook (( prog-mode text-mode ) . idle-highlight-mode ))
( add-hook 'after-change-major-mode-hook
  ( lambda ( )
    ( when ( derived-mode-p 'c-mode )
      ( setq-local idle-highlight-exceptions '( "unsigned" "signed" "long" "int" "shot" "char" )))
    ( when ( derived-mode-p 'python-mode )
      ( setq-local idle-highlight-exceptions '( "list" "tuple" "int" "float" "str" "bool" )))))

( use-package column-enforce-mode )
( global-column-enforce-mode t )
( setq column-enforce-comments t )


( use-package paredit )
(autoload 'enable-paredit-mode  "paredit"   "Ative a edição pseudo-estrutural do código Lisp."  t)
( add-hook 'emacs-lisp-mode-hook #'enable-paredit-mode )
( add-hook 'eval-expression-minibuffer-setup-hook #'enable-paredit-mode )
( add-hook 'ielm-mode-hook #'enable-paredit-mode )
( add-hook 'lisp-mode-hook #'enable-paredit-mode )
( add-hook 'lisp-interaction-mode-hook #'enable-paredit-mode )
( add-hook 'scheme-mode-hook #'enable-paredit-mode )
( eldoc-add-command
  'paredit-backward-delete
  'paredit-close-round )
( add-hook 'slime-repl-mode-hook ( lambda ( ) ( paredit-mode +1 )))


( use-package ligature
  :load-path "path-to-ligature-repo"
  :config
  ;;; Enable the "www" ligature in every possible major mode
  ( ligature-set-ligatures 't '( "www" ))
  ;;; Enable traditional ligature support in eww-mode, if the
  ;;; `variable-pitch' face supports it
  ( ligature-set-ligatures 'eww-mode '( "ff" "fi" "ffi" ))
  ;;; Enable all Cascadia Code ligatures in programming modes
  ( ligature-set-ligatures 'prog-mode '("|||>" "<|||" "<==>" "<!--" "####" "~~>" "***" "||=" "||>"
                                        ":::" "::=" "=:=" "===" "==>" "=!=" "=>>" "=<<" "=/=" "!=="
                                        "!!." ">=>" ">>=" ">>>" ">>-" ">->" "->>" "-->" "---" "-<<"
                                        "<~~" "<~>" "<*>" "<||" "<|>" "<$>" "<==" "<=>" "<=<" "<->"
                                        "<--" "<-<" "<<=" "<<-" "<<<" "<+>" "</>" "###" "#_(" "..<"
                                        "..." "+++" "/==" "///" "_|_" "www" "&&" "^=" "~~" "~@" "~="
                                        "~>" "~-" "**" "*>" "*/" "||" "|}" "|]" "|=" "|>" "|-" "{|"
                                        "[|" "]#" "::" ":=" ":>" ":<" "$>" "==" "=>" "!=" "!!" ">:"
                                        ">=" ">>" ">-" "-~" "-|" "->" "--" "-<" "<~" "<*" "<|" "<:"
                                        "<$" "<=" "<>" "<-" "<<" "<+" "</" "#{" "#[" "#:" "#=" "#!"
                                        "##" "#(" "#?" "#_" "%%" ".=" ".-" ".." ".?" "+>" "++" "?:"
                                        "?=" "?." "??" ";;" "/*" "/=" "/>" "//" "__" "~~" "(*" "*)"
                                        "\\\\" "://" )))
  ;;; Enables ligature checks globally in all buffers. You can also do it
  ;;; per mode with `ligature-mode'.

( use-package highlight-numbers
  :init
  ( add-hook 'prog-mode-hook 'highlight-numbers-mode)
  :config )

( use-package auto-highlight-symbol )
( global-auto-highlight-symbol-mode t )


;;; Settings packages local

( setq lambda-symbol ( string #x1d77a ))

;;; folding > && M-x occur && Mx folding-mode on
( if ( require 'folding nil 'noerror )
    ( folding-mode-add-find-file-hook )
  ( message "Library `folding' not found" ))



;;; function

( defun electric-pair ( )
  "If at end of line, insert character pair without surrounding spaces.
    Otherwise, just insert the typed character."
  ( interactive )
  ( if ( eolp)  ( let ( parens-require-spaces ) ( insert-pair )) ( self-insert-command 1 )))


;; Python auto pairs
(add-hook 'python-mode-hook
          (lambda ()
            (define-key python-mode-map "\"" 'electric-pair)
            (define-key python-mode-map "\'" 'electric-pair)
            (define-key python-mode-map "(" 'electric-pair)
            (define-key python-mode-map "[" 'electric-pair)
            (define-key python-mode-map "{" 'electric-pair)))



(defun display-code-line-counts (ov)
  (when (eq 'code (overlay-get ov 'hs))
    (overlay-put ov 'help-echo
                 (buffer-substring (overlay-start ov)
 		                           (overlay-end ov)))))

(setq hs-set-up-overlay 'display-code-line-counts)


;; Lambda icon
(defun my-pretty-lambda ()
  "make some word or string show as pretty Unicode symbols"
  (setq prettify-symbols-alist
        '(
          ("lambda" . 955) ; λ
          )))

(add-hook 'scheme-mode-hook 'my-pretty-lambda)
(global-prettify-symbols-mode 1)




;; Python Syntax icons
(global-prettify-symbols-mode 1)

(add-hook
 'python-mode-hook
 (lambda ()
   (mapc (lambda (pair) (push pair prettify-symbols-alist))
         '(;; Syntax
           ("def" .      #x2131)
           ("not" .      #x2757)
           ("in" .       #x2208)
           ("not in" .   #x2209)
           ("return" .   #x27fc)
           ("yield" .    #x27fb)
           ("for" .      #x2200)
           ;; Base Types
           ("int" .      #x2124)
           ("float" .    #x211d)
           ("str" .      #x1d54a)
           ("True" .     #x1d54b)
           ("False" .    #x1d53d)
           ;; Mypy
           ("Dict" .     #x1d507)
           ("List" .     #x2112)
           ("Tuple" .    #x2a02)
           ("Set" .      #x2126)
           ("Iterable" . #x1d50a)
           ("Any" .      #x2754)
           ("Union" .    #x22c3)))))

;; C syntax icons
(global-prettify-symbols-mode 1)
(add-hook
 'c-mode-hook
 (lambda ()
   (mapc (lambda (pair) (push pair prettify-symbols-alist))
         '(;; Syntax
           ("return" .   #x27fc)
           ("for" .      #x2200)
                                        ; Base Types
           ("int" .      #x2124)
           ("float" .    #x211d)
           ("char" . #x2102)
           ("unsigned" . #x1D54C)
           ("long" . #x1D543)
           ("short" . #x1D54A)
           ("void" . #x1D729)
           ("while" . #x1E00)
           ("do" . "ζ")
           ("#include" . #x0552)
           ("#define" . #x2132)
           ("if" . #x2208)
           ("else" . #x2209)
           ("else if" . #x220B)
           ("printf" . "℞")
           ("main" . "∝")))))




;; Treesitter
(functionp 'module-load) ; should be t
module-file-suffix ; should be non-nil
(global-tree-sitter-mode)
(add-hook 'tree-sitter-after-on-hook #'tree-sitter-hl-mode)
;; https://robbmann.io/posts/emacs-treesit-auto/
(setq treesit-language-source-alist
      '((bash "https://github.com/tree-sitter/tree-sitter-bash")
        (c "https://github.com/tree-sitter/tree-sitter-c")
        (clojure "https://github.com/sogaiu/tree-sitter-clojure.git")
        (cmake "https://github.com/uyha/tree-sitter-cmake")
        (commonlisp "https://github.com/thehamsta/tree-sitter-commonlisp")
        (cpp "https://github.com/tree-sitter/tree-sitter-cpp")
        (css "https://github.com/tree-sitter/tree-sitter-css")
        (c-sharp "https://github.com/tree-sitter/tree-sitter-c-sharp")
        (dockerfile "https://github.com/camdencheek/tree-sitter-dockerfile")
        (elisp "https://github.com/Wilfred/tree-sitter-elisp")
        (go "https://github.com/tree-sitter/tree-sitter-go")
        (gomod "https://github.com/camdencheek/tree-sitter-go-mod")
        (html "https://github.com/tree-sitter/tree-sitter-html")
        (java "https://github.com/tree-sitter/tree-sitter-java")
        (javascript "https://github.com/tree-sitter/tree-sitter-javascript" "master" "src")
        (json "https://github.com/tree-sitter/tree-sitter-json")
        (latex "https://github.com/latex-lsp/tree-sitter-latex")
        (lua "https://github.com/Azganoth/tree-sitter-lua")
        (make "https://github.com/alemuller/tree-sitter-make")
        (markdown "https://github.com/ikatyang/tree-sitter-markdown")
        (python "https://github.com/tree-sitter/tree-sitter-python")
        (r "https://github.com/r-lib/tree-sitter-r")
        (rust "https://github.com/tree-sitter/tree-sitter-rust")
        (toml "https://github.com/tree-sitter/tree-sitter-toml")
        (tsx . ("https://github.com/tree-sitter/tree-sitter-typescript" "master" "tsx/src"))
        (typescript . ("https://github.com/tree-sitter/tree-sitter-typescript" "master" "typescript/src"))
        (yaml "https://github.com/ikatyang/tree-sitter-yaml")
        (zig "https://github.com/GrayJack/tree-sitter-zig")))

(setq major-mode-remap-alist
      '((sh-mode . bash-ts-mode)
        (clojurec-mode . clojure-ts-mode)
        (clojurescript-mode . clojure-ts-mode)
        (clojure-mode . clojure-ts-mode)
        (css-mode . css-ts-mode)
        (go-mode . go-ts-mode)
        (go-dot-mod-mode . go-mod-ts-mode)
        (mhtml-mode . html-ts-mode)
        (sgml-mode . html-ts-mode)
        (java-mode . java-ts-mode)
        (js-mode . js-ts-mode)
        (javascript-mode . js-ts-mode)
        (js-json-mode . json-ts-mode)
        (python-mode . python-ts-mode)
        (yaml-mode . yaml-ts-mode)))

(defun install-latest-known-treesitter-grammars ()
  "Install/upgrade all latest Tree-sitter grammars."
  (interactive)
  (dolist (grammar treesit-language-source-alist)
    (message "Downloading %s treesitter grammar from %s" (car grammar) (cadr grammar))
    (treesit-install-language-grammar (car grammar))))

;; Disable line numbers for some modes
(dolist (mode '(org-mode-hook
                term-mode-hook
		        shell-mode-hook
                eshell-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

(defun line-to-top-of-window ()
  "Move the line point is on to top of window."
  (interactive)
  (recenter 0))
(global-set-key [f6] 'line-to-top-of-window)

(defadvice query-replace-read-args (before barf-if-buffer-read-only activate)
  "Signal a `buffer-read-only' error if the current buffer is read-only."
  (barf-if-buffer-read-only))

(defun m-shell-command ()
  "Launch a shell command."
  (interactive)
  (let ((command (read-string "Command: ")))
    (shell-command (concat command " &") (concat "*" command "*"))))



(defconst animate-n-steps 3)
(defun emacs-reloaded ()
  (animate-string (concat ";; Initialization successful, welcome to "
                          (substring (emacs-version) 0 16)
                          ".")
                  0 0)
  (newline-and-indent)  (newline-and-indent))


;; Disable line numbers for some modes
(dolist (mode '(org-mode-hook
                term-mode-hook
		        shell-mode-hook
                eshell-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

(defun line-to-top-of-window ()
  "Move the line point is on to top of window."
  (interactive)
  (recenter 0))
(global-set-key [f6] 'line-to-top-of-window)

(defadvice query-replace-read-args (before barf-if-buffer-read-only activate)
  "Signal a `buffer-read-only' error if the current buffer is read-only."
  (barf-if-buffer-read-only))

(defun m-shell-command ()
  "Launch a shell command."
  (interactive)
  (let ((command (read-string "Command: ")))
    (shell-command (concat command " &") (concat "*" command "*"))))


(defconst animate-n-steps 3)
(defun emacs-reloaded ()
  (animate-string (concat ";; Initialization successful, welcome to "
                          (substring (emacs-version) 0 16)
                          ".")
                  0 0)
  (newline-and-indent)  (newline-and-indent))


;; Function Backup
(setq
 backup-by-copying t      ; don't clobber symlinks
 backup-directory-alist
 '(("." . "~/.saves/"))    ; don't litter my fs tree
 delete-old-versions t
 kept-new-versions 6
 kept-old-versions 2
 version-control t)       ; use versioned backups

;; make-backup-file-name-function

(setq backup-directory-alist
      `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))


;; Save files
(defun full-auto-save ()
  (interactive)
  (save-excursion
    (dolist (buf (buffer-list))
      (set-buffer buf)
      (if (and (buffer-file-name) (buffer-modified-p))
          (basic-save-buffer)))))
(add-hook 'auto-save-hook 'full-auto-save)

(defun save-all ()
  (interactive)
  (save-some-buffers t))

(add-hook 'focus-out-hook 'save-all)

;; Force Backup
(setq version-control t ;; Use version numbers for backups.
      kept-new-versions 10 ;; Number of newest versions to keep.
      kept-old-versions 0 ;; Number of oldest versions to keep.
      delete-old-versions t ;; Don't ask to delete excess backup versions.
      backup-by-copying t) ;; Copy all files, don't rename them.

;; Default and per-save backups go here:
(setq backup-directory-alist '(("" . "~/.emacs.d/backup/per-save")))

(defun force-backup-of-buffer ()
  ;; Make a special "per session" backup at the first save of each
  ;; emacs session.
  (when (not buffer-backed-up)
    ;; Override the default parameters for per-session backups.
    (let ((backup-directory-alist '(("" . "~/.emacs.d/backup/per-session")))
          (kept-new-versions 3))
      (backup-buffer)))
  ;; Make a "per save" backup on each save.  The first save results in
  ;; both a per-session and a per-save backup, to keep the numbering
  ;; of per-save backups consistent.
  (let ((buffer-backed-up nil))
    (backup-buffer)))
(add-hook 'before-save-hook  'force-backup-of-buffer)

(setq backup-directory-alist
      `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))

;; Files
;; Recover Files
;; M-x recover-file == M-x auto-save-visited-mode on
(setq backup-directory-alist
      `(("." . ,(concat user-emacs-directory "backups"))))

;; Backup
(setq
 backup-by-copying t      ; don't clobber symlinks
 backup-directory-alist
 '(("." . "~/.saves/"))    ; don't litter my fs tree
 delete-old-versions t
 kept-new-versions 6
 kept-old-versions 2
 version-control t)       ; use versioned backups


;; Minimize garbage collection during startup
(setq gc-cons-threshold most-positive-fixnum)

;; Lower threshold back to 8 MiB (default is 800kB)
(add-hook 'emacs-startup-hook
          (lambda ()
            (setq gc-cons-threshold (expt 2 23))))

;; Use a hook so the message doesn't get clobbered by other messages.
(add-hook 'emacs-startup-hook
          (lambda ()
            (message "Emacs ready in %s with %d garbage collections."
                     (format "%.2f seconds"
                             (float-time
                              (time-subtract after-init-time before-init-time)))
                     gcs-done)))


;; Change cursor color according to mode
(defvar hcz-set-cursor-color-color "")
(defvar hcz-set-cursor-color-buffer "")
(defun hcz-set-cursor-color-according-to-mode ()
  "change cursor color according to some minor modes."
  ;; set-cursor-color is somewhat costly, so we only call it when needed:
  (let ((color
         (if buffer-read-only "white"
           (if overwrite-mode "red"
             "yellow"))))
    (unless (and
             (string= color hcz-set-cursor-color-color)
             (string= (buffer-name) hcz-set-cursor-color-buffer))
      (set-cursor-color (setq hcz-set-cursor-color-color color))
      (setq hcz-set-cursor-color-buffer (buffer-name)))))
(add-hook 'post-command-hook 'hcz-set-cursor-color-according-to-mode)


(defun my-mark-eob ()
  (let ((existing-overlays (overlays-in (point-max) (point-max)))
	    (eob-mark (make-overlay (point-max) (point-max) nil t t))
	    (eob-text "~~~"))
    ;; Delete any previous EOB markers.  Necessary so that they don't
    ;; accumulate on calls to revert-buffer.
    (dolist (next-overlay existing-overlays)
      (if (overlay-get next-overlay 'eob-overlay)
	      (delete-overlay next-overlay)))
    ;; Add a new EOB marker.
    (put-text-property 0 (length eob-text)
			           'face '(foreground-color . "slate gray") eob-text)
    (overlay-put eob-mark 'eob-overlay t)
    (overlay-put eob-mark 'after-string eob-text)))
(add-hook 'find-file-hooks 'my-mark-eob)



(make-face 'foo)
(set-face-stipple 'foo (list 2 2 (string 1 2)))
(let ((str "Foobar -> BrainFuck"))
  (put-text-property 0 3 'face 'foo str)
  (insert str))

                                        ; could be bad, will not let you save at all, until you correct the error
(add-hook 'emacs-lisp-mode-hook
          (lambda ()
            (add-hook 'local-write-file-hooks
                      'check-parens)))

(defun my-move-function-up ()
  "Move current function up."
  (interactive)
  (save-excursion
    (c-mark-function)
    (let ((fun-beg (point))
          (fun-end (mark)))
      (transpose-regions (progn
                           (c-beginning-of-defun 1)
                           (point))
                         (progn
                           (c-end-of-defun 1)
                           (point))
                         fun-beg fun-end))))

(defun my-move-function-down ()
  "Move current function down."
  (interactive)
  (save-excursion
    (c-mark-function)
    (let ((fun-beg (point))
          (fun-end (mark)))
      (transpose-regions fun-beg fun-end
                         (progn
                           (c-beginning-of-defun -1)
                           (point))
                         (progn
                           (c-end-of-defun 1)
                           (point))))))

(defun mark-c-scope-beg ()
  "Marks the c-scope (region between {}) enclosing the point.
   Naive, as will be confused by { } within strings"
  (let
	  ((scope-depth 1))
	(while (not (= scope-depth 0))
	  (search-backward-regexp "}\\|{")
	  (if (string= (char-to-string (char-after)) "}")
		  (setq scope-depth (1+ scope-depth))
		(setq scope-depth (1- scope-depth)))))
  (point))

(defun mark-c-scope-end ()
  "Marks the c-scope (region between {}) enclosing the point.
   Naive, as will be confused by { } within strings"
  (let
	  ((scope-depth 1))
	(while (not (= scope-depth 0))
	  (search-forward-regexp "}\\|{")
	  (if (string= (char-to-string (char-before)) "}")
		  (setq scope-depth (1- scope-depth))
		setq scope-depth (1+ scope-depth))))
  (point))

(defun kill-c-scope ()
  (interactive)
  (let
	  ((inital-point (point)))
	(save-excursion
	  (let
		  ((beg (mark-c-scope-beg)))
		(goto-char inital-point)
		(let ((end (mark-c-scope-end))))))))


(defun dka-fix-comments ()
  "Move through the entire buffer searching for comments that begin with
    \"//\" and fill them appropriately.  That means if comments start too
    close to the end of line (20 less than the fill-column) move the
    entire comment on a line by itself."
  (interactive)
  (save-excursion
    (beginning-of-buffer)
    (while (search-forward "//")
      (lisp-indent-for-comment)
      ;; when the comment is deemed to be too close to the end of the
      ;; line, yank it and put it on the previous line before filling
      (while (< (- fill-column 20) (- (current-column) 3))
	    (search-backward "//")
	    (kill-line)
	    (beginning-of-line)
	    (yank)
	    (insert "\n"))
      ;; now fill the lines that are too long
      (if (and (not (end-of-line))
               (< fill-column (current-column)))
          (c-fill-paragraph)))))


(defun whack-typedefs ()
  "Convert typedefs to structs."
  (interactive)
  (let ((typedefs-buffer (or (get-buffer "typedefs.h")
                             (find-file-noselect "typedefs.h")))
        name typedefs)
    (save-excursion
      (goto-char (point-min))
      (while (re-search-forward "^typedef \\(struct\\|enum\\) ?\\(\\(?:\\w\\|\\s_\\)+\\)?[ \t\n]*{" nil t)
        (setq keyword (match-string 1))
        (setq name (match-string 2))
        (if name
            (progn ;must be of the format "typedef struct foo_ {\n ... \n} foo;"
              (when (equal (substring name -1) "_")
                (setq name (substring name 0 -1)))
              (replace-match (concat keyword " " (downcase name) " {") t nil)
              (goto-char (- (point) 1))
              (forward-sexp)
              (kill-line)
              (insert ";"))
                                        ; must be of the format "typedef struct\n{ ... }\nfoo;
          (goto-char (- (point) 1))
          (forward-sexp)
          (when (re-search-forward "\\(\\(?:\\w\\|\\s_\\)+\\);" nil t)
            (setq name (match-string 1))
            (kill-entire-line)
            (goto-char (- (point) 1))
            (insert ";")
            (backward-sexp)
            (delete-indentation)
            (insert (concat " " (downcase name)))))
        (when name
          (save-excursion
            (goto-char (point-min))
            (while (re-search-forward (concat "\\([^_]\\)" name "\\([^_]\\)") nil t)
              (replace-match
               (concat (match-string 1)
                       (when (and keyword (equal keyword "struct"))
                         (concat "struct "))
                       (downcase name) (match-string 2)) t nil)))
          (with-current-buffer typedefs-buffer
            (insert (concat "typedef "
                            (when (and keyword (equal keyword "struct"))
                              (concat "struct "))
                            (downcase name) " " name "\n")))
          (setq name nil))))))


;; Autostart body code C
(eval-after-load 'autoinsert
  '(define-auto-insert '("\\.c\\'" . "C skeleton")
     '(
       "Short description: "
       "/**\n * "
       (file-name-nondirectory (buffer-file-name))
       " -- " str \n
       " *" \n
       " * Written on " (format-time-string "%A, %e %B %Y.") \n
       " */" > \n \n
       "#include <stdio.h>" \n
       "#include \""
       (file-name-sans-extension
        (file-name-nondirectory (buffer-file-name)))
       ".h\"" \n \n
       "int main()" \n
       "{" > \n
       > _ \n
       "}" > \n)))

(defun lispy-parens ()
  "Setup parens display for lisp modes"
  (setq show-paren-delay 0)
  (setq show-paren-style 'parenthesis)
  (make-variable-buffer-local 'show-paren-mode)
  (show-paren-mode 1)
  (set-face-background 'show-paren-match-face (face-background 'default))
  (if (boundp 'font-lock-comment-face)
      (set-face-foreground 'show-paren-match-face
     			           (face-foreground 'font-lock-comment-face))
    (set-face-foreground 'show-paren-match-face
     			         (face-foreground 'default)))
  (set-face-attribute 'show-paren-match-face nil :weight 'extra-bold))


(defvar my-wacky-syntax-table
  (let ((table (make-syntax-table)))
    (modify-syntax-entry ?[ "w" table)
                         (modify-syntax-entry ?] "w" table)
    table))

(global-set-key "\C-\M-f" '(lambda ()
                             (interactive)
                             (with-syntax-table my-wacky-syntax-table (forward-sexp))))
(set-syntax-table my-wacky-syntax-table)

;; Stop SLIME's REPL from grabbing DEL,
;; which is annoying when backspacing over a '('
(defun override-slime-repl-bindings-with-paredit ()
  (define-key slime-repl-mode-map
    (read-kbd-macro paredit-backward-delete-key) nil))
(add-hook 'slime-repl-mode-hook 'override-slime-repl-bindings-with-paredit)

(defvar electrify-return-match
  "[\]}\)\"]"
  "If this regexp matches the text after the cursor, do an \"electric\"
  return.")

(defun electrify-return-if-match (arg)
  "If the text after the cursor matches `electrify-return-match' then
  open and indent an empty line between the cursor and the text.  Move the
  cursor to the new line."
  (interactive "P")
  (let ((case-fold-search nil))
    (if (looking-at electrify-return-match)
        (save-excursion (newline-and-indent)))
    (newline arg)
    (indent-according-to-mode)))

;; Using local-set-key in a mode-hook is a better idea.
(global-set-key (kbd "RET") 'electrify-return-if-match)

(add-hook 'emacs-lisp-mode-hook
          (lambda ()
            (paredit-mode t)
            (turn-on-eldoc-mode)
            (eldoc-add-command
             'paredit-backward-delete
             'paredit-close-round)
            (local-set-key (kbd "RET") 'electrify-return-if-match)
            (eldoc-add-command 'electrify-return-if-match)
            (show-paren-mode t)))

(progn
  (quail-define-package
   "easy-parentheses" "Latin-1" "EP<" t
   "Easy parentheses input method (rule: ,, -> (   .. -> )))

Doubling the postfix separates the letter and postfix: e.g. ,,, -> ,,
" nil t nil nil nil nil nil nil nil nil t)

  (quail-define-rules
   (",," ?()
    (".." ?))
   (",,," [",,"])
   ("..." [".."])
   ))

(global-set-key "]" 'universal-close-paren)
(defvar ucp-open-re
  "[[({]"
  "Regexp matching open raw parens")
(defvar ucp-close-re
  "[])}]"                               ;close bracket must come first
  "Regexp matching close raw parens.")

(defun ucp-lp2rp (lhs)
  (cond ((string-equal lhs "(") ")")    ;cannot use case because of eql
	    ((string-equal lhs "{") "}")
	    ((string-equal lhs "[") "]")
	    ((string-equal lhs "\\(") "\\)")
	    ((string-equal lhs "\\{") "\\}")
	    ((string-equal lhs "\\[") "\\]")
	    (t (concat "ucp-lp2rp unexpected: " lhs))))
(defun ucp-rp2lp (rhs)
  (cond ((string-equal rhs ")") "(")
	    ((string-equal rhs "}") "{")
	    ((string-equal rhs "]") "[")
	    ((string-equal rhs "\\)") "\\(")
	    ((string-equal rhs "\\}") "\\{")
	    ((string-equal rhs "\\]") "\\[")
	    (t (concat "ucp-rp2lp unexpected: " rhs))))
(defun universal-close-paren (parg)
  "Insert the \"correct\" close paren.
Use C-u to override."
  (interactive "P")
  (if parg
      (self-insert-command 1)             ;override
    (let ((rightmost-paren
	       (save-excursion (save-match-data (ucp-pda 0 "")))))
      (if (= (car rightmost-paren) 0)
	      ;; problem detected
	      (if (string-equal (cdr rightmost-paren) "")
	          ;; no open detected
	          (message "Parens are already balanced up to this point.")
	        ;; extra close detected: long else
	        (goto-char (string-to-int (cdr rightmost-paren)))
	        (message "Extra close paren detected here."))
	    ;; car is point of interest
	    (if (string-match ucp-open-re (cdr rightmost-paren))
	        ;; match: we want to insert rp
	        (let ((rp (ucp-lp2rp (cdr rightmost-paren))))
	          ;; detect if we have already manually escaped by ourselves
	          (if (and
		           (string-equal (substring rp 0 1) "\\") ;rp is escaped
		           (= (preceding-char) ?\\) ;point is not right after \\
		           (/= (char-before (1- (point))) ?\\))
	              (insert (substring rp 1)) ;skip first char
	            (insert rp)))             ;no skip
	      ;; mismatch detected: long else
	      (goto-char (car rightmost-paren))
	      (message
	       (concat "Mismatched open paren detected here. Expecting: "
		           (ucp-rp2lp (cdr rightmost-paren)))))))))
(defun ucp-pda (level expected-open)  ;return point of rightmost unclosed open
  "Use the recursion stack to perform paren matching.
May run into limits for large files. Try something like:
\(setq max-lisp-eval-depth \(* 64 1048576\)\)
\(setq max-specpdl-size \(* 64 1048576\)\)

LEVEL is the number of close paren we have seen so far.
EXPECTED-OPEN is what we are expected to see if we do see an open paren.

On non-zero level, return nil when match found.
Otherwise, return
  (0.\"\") if goes pass point-min
  (0.\"point\") if extra close detected at point
  (point.\"open\") if point is the matched open
  (point.\"close\") if open at point mismatches close"
  (let ((rightmost-paren
	     (and (re-search-backward
	           (concat "\\(" ucp-open-re "\\|" ucp-close-re "\\)")
	           (point-min) 't)
	          (cons (point) (match-string 0)))))
    (if (not rightmost-paren)
        ;; no more paren
        (cons 0 (if (= level 0)           ;walk passed point-min
		            ""                      ;=> 0.""
		          t))                     ;=> 0.t (t will be replaced by oldpt)
      ;; hit a paren, long else
      (when (re-search-backward "\\\\" (- (point) 1) t)
	    (setq rightmost-paren           ;patch rightmost-paren if quoted
	          (cons (point) (concat "\\" (cdr rightmost-paren)))))
      (let ((p (cdr rightmost-paren)))
	    (if (string-match ucp-close-re p) ;note the patch above
	        ;; hit a close paren
	        (let ((oldpt (point))         ;at the closing paren
		          (recur                  ;eat until the matching open
		           (ucp-pda (1+ level) (ucp-rp2lp p))))
	          (if recur
	              ;; not nil => mismatch and report point
	              (if (= (car recur) 0)
		              (cons 0 (number-to-string oldpt)) ;0.t => 0."oldpt"
		            ;; if pt.nil => pt."close", otherwise => keep
		            (if (cdr recur) recur (cons (point) p)))
	            ;; nil => match, restart scanning at same level
	            (ucp-pda level expected-open)))
	      ;; hit an open paren
	      (if (= level 0)
	          rightmost-paren             ;=>pt.open; found open at level 0!
	        (if (string-equal expected-open p)
	            nil                       ;match => nil
	          (cons (point) nil)))))))) ;miss => pt.nil (nil will be replaced)
                                        ;but note: nil will become (lp2rp expected-open)

(defun close-quoted-open-paren-right (args)
  (interactive "P")
  (setq args (or args 1))
  (dotimes (k args)
    (let (pos closing)
      (with-syntax-table all-paren-syntax-table
        (setq pos (save-excursion (up-list -1) (point)))
        (setq closing (matching-paren (char-after pos))))
      (message "The matched paren pos is %d" pos)
      (while (eq (char-before pos) ?\\)
        (insert "\\")
        (setq pos (1- pos)))
      (insert closing)))
  t)

(defun close-quoted-open-paren-left (args)
  (interactive "P")
  (setq args (or args 1))
  (dotimes (k args)
    (let (pos closing (i 1))
      (with-syntax-table all-paren-syntax-table
        (setq pos (save-excursion (up-list 1) (point)))
        (setq closing (matching-paren (char-before pos))))
      (while (eq (char-before (- pos i)) ?\\)
        (setq i (1+ i)))
      (dotimes (j (1- i)) (insert "\\")) ;;insert after to count right
      (insert closing)
      (message "The matched paren pos is %d" (+ pos i))
      (backward-char i)))
  t)

(defun close-all-open-paren-right ()
  (interactive)
  (while  (ignore-errors (close-quoted-open-paren-right 1))))

(defun close-all-open-paren-left ()
  (interactive)
  (while  (ignore-errors (close-quoted-open-paren-left 1))))


(defun replace-regexp-in-region (start end)
  (interactive "*r")
  (save-excursion
    (save-restriction
      (let ((regexp (read-string "Regexp: "))
            (to-string (read-string "Replacement: ")))
        (narrow-to-region start end)
        (goto-char (point-min))
        (while (re-search-forward regexp nil t)
          (replace-match to-string nil nil))))))

(defun myscope/disabled-commands-in-custom-file (orig-fun &rest orig-args)
  "Put declarations in `custom-file'."
  (let ((user-init-file custom-file))
    (apply orig-fun orig-args)))
(advice-add 'en/disable-command :around #'myscope/disabled-commands-in-custom-file)

(defvar hs-special-modes-alist
  (mapcar 'purecopy
          '((c-mode "{" "}" "/[*/]" nil nil)
            (c++-mode "{" "}" "/[*/]" nil nil)
            (bibtex-mode ("@\\S(*\\(\\s(\\)" 1))
            (java-mode "{" "}" "/[*/]" nil nil)
            (js-mode "{" "}" "/[*/]" nil))))

(defun toggle-selective-display (column)
  (interactive "P")
  (set-selective-display
   (or column
       (unless selective-display
         (1+ (current-column))))))

(defun toggle-hiding (column)
  (interactive "P")
  (if hs-minor-mode
      (if (condition-case nil
              (hs-toggle-hiding)
            (error t))
          (hs-show-all))
    (toggle-selective-display column)))
(add-hook 'c-mode-common-hook   'hs-minor-mode)
(add-hook 'emacs-lisp-mode-hook 'hs-minor-mode)
(add-hook 'java-mode-hook       'hs-minor-mode)
(add-hook 'lisp-mode-hook       'hs-minor-mode)
(add-hook 'perl-mode-hook       'hs-minor-mode)
(add-hook 'sh-mode-hook         'hs-minor-mode)

(defun display-code-line-counts (ov)
  (when (eq 'code (overlay-get ov 'hs))
    (overlay-put ov 'help-echo
                 (buffer-substring (overlay-start ov)
 		                           (overlay-end ov)))))

(setq hs-set-up-overlay 'display-code-line-counts)

(defun hs-hide-leafs-recursive (minp maxp)
  "Hide blocks below point that do not contain further blocks in
    region (MINP MAXP)."
  (when (hs-find-block-beginning)
    (setq minp (1+ (point)))
    (funcall hs-forward-sexp-func 1)
    (setq maxp (1- (point))))
  (unless hs-allow-nesting
    (hs-discard-overlays minp maxp))
  (goto-char minp)
  (let ((leaf t))
    (while (progn
             (forward-comment (buffer-size))
             (and (< (point) maxp)
                  (re-search-forward hs-block-start-regexp maxp t)))
      (setq pos (match-beginning hs-block-start-mdata-select))
      (if (hs-hide-leafs-recursive minp maxp)
          (save-excursion
            (goto-char pos)
            (hs-hide-block-at-point t)))
      (setq leaf nil))
    (goto-char maxp)
    leaf))

(defun hs-hide-leafs ()
  "Hide all blocks in the buffer that do not contain subordinate
    blocks.  The hook `hs-hide-hook' is run; see `run-hooks'."
  (interactive)
  (hs-life-goes-on
   (save-excursion
     (message "Hiding blocks ...")
     (save-excursion
       (goto-char (point-min))
       (hs-hide-leafs-recursive (point-min) (point-max)))
     (message "Hiding blocks ... done"))
   (run-hooks 'hs-hide-hook)))


;; Highlight some words
(font-lock-add-keywords
 nil '(("\\<\\(FIXME\\|TODO\\|BUG\\)" 1 font-lock-warning-face t)))

;; Spell checker
(setq-default ispell-program-name "aspell")

;; Localization (using UTF-8 by default)
(set-keyboard-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)
(prefer-coding-system 'utf-8)
(set-language-environment "UTF-8")
(set-terminal-coding-system 'utf-8)
(set-language-environment 'utf-8)


;; bitblee uses K&R with tabs and 120 columns
(add-hook 'c-mode-hook 'asc:init-c-mode)
(defun asc:init-c-mode ()
  (idle-highlight-mode 1)
  (add-hook 'before-save-hook 'delete-trailing-whitespace t t)
  (define-abbrev c-mode-abbrev-table "fu" "" 'asc:c-comment-function)
  (define-abbrev c-mode-abbrev-table "co" "" 'asc:c-comment)
  (setq c-basic-offset 4
	    tab-width 4
	    fill-column 120))

(eval-after-load "cc-vars"
  (lambda ()
    (add-to-list 'c-default-style '(c-mode . "k&r"))))

(setq tags-revert-without-query t)

(define-skeleton asc:c-comment-function
  "Insert a multi-line comment.

/**
 * Like this.
 */"
  nil
  "/**\n"
  " * " _ "\n"
  " */")

(define-skeleton asc:c-comment
  "Insert a single-line comment. /* like this */"
  nil "/* " _ " */")

(setq whitespace-style '(face trailing))
(global-whitespace-mode)
(setq show-trailing-whitespace t
      sentence-end-double-space nil
      default-fill-column 80)
(add-hook 'before-save-hook 'delete-trailing-whitespace)
(setq backward-delete-char-untabify-method 'hungry)

(setq-default scroll-margin 5
              scroll-conservatively 0
              scroll-up-aggressively 0.01
              scroll-down-aggressively 0.01)

(defvar mastodon-documentation
  '(("https://developer.gnome.org/glib/stable/glib-String-Utility-Functions.html#top"
     . "*String Utility Functions*")
    ("https://github.com/tootsuite/documentation/blob/master/Using-the-API/API.md#start-of-content"
     . "*Mastodon API*")
    ("https://developer.gnome.org/glib/stable/glib-Strings.html#top" . "*Strings*")
    ("https://developer.gnome.org/glib/stable/glib-Unicode-Manipulation.html#top"
     . "*Unicode Manipulation*")
    ("https://developer.gnome.org/glib/stable/glib-Singly-Linked-Lists.html#top" . "*Single Linked Lists*")
    ("https://developer.gnome.org/glib/stable/glib-Hash-Tables.html#top" . "*Hash Tables*"))
  "List of cons cells with doc URL and buffer name.")

(defun mastodon-documentation ()
  "Load all the documentation documents."
  (interactive)
  (dolist (cell mastodon-documentation)
    (unless (get-buffer (cdr cell))
      (set-buffer (get-buffer-create (cdr cell)))
      ;; this makes eww reuse the buffer we just created
      (eww-mode)
      (eww (car cell)))))

(setq c-style-variables-are-local-p t)

(defun my-c-mode-hooks ()
  (let ((bname (buffer-file-name)))
    (cond
     ((string-match "tools_src/" bname) (c-set-style "gnu"))
     ((string-match "uClinux/" bname) (c-set-style "linux"))
     ((string-match "pump/" bname) (c-set-style "gnu"))
     ((string-match "dhcp-" bname) (c-set-style "gnu"))
     ((string-match "ipconfd/" bname) (c-set-style "gnu"))
     ((string-match "gnu/emacs/" bname) (gnu-tabs))
     ((string-match "nn-tk/" bname) (tab8))
     ((string-match "gnu" bname) t)
     ((string-match "\\.[ch]$" bname) (c-set-style "gnu"))
     )))

(add-hook 'c-mode-hook 'my-c-mode-hooks)


(add-hook 'c-mode-common-hook
          (lambda ()
            (font-lock-add-keywords nil
                                    '(("\\<\\(FIXME\\|TODO\\|BUG\\):" 1 font-lock-warning-face t)))))

;; Sometimes, I want to see trailing whitespace
(defun fc-turn-on-show-trailing-whitespace ()
  "Set `show-trailing-whitespace' to t."
  (setq show-trailing-whitespace t))

(mapc (lambda (hook)
        (add-hook hook 'fc-turn-on-show-trailing-whitespace))
      '(text-mode-hook
        scheme-mode-hook
        emacs-lisp-mode-hook
        java-mode-hook
        c-mode-hook))



;;; c-mode
(add-hook 'c-mode-hook 'fc-c-setup)
(defun fc-c-setup ()
  "Set up C mode for my needs."
  (c-set-style "k&r")
  (setq c-basic-offset 5)
  (c-set-offset 'case-label '+)
  (c-set-offset 'inextern-lang 0))

(setq special-display-buffer-names
      '("*compilation*"))

(defun my-space-as-tab ()
  "Insert spaces as tabs."
  (interactive)
  (setq-default indent-tabs-mode nil)
  (setq indent-line-function 'insert-tab)
  )



(add-hook 'c++-mode-hook
          (lambda ()
            (flyspell-prog-mode)
                                        ; ...
            ))

;;; Java mode
 (add-hook 'java-mode-hook
              (lambda ()
                "Treat Java 1.5 @-style annotations as comments."
                (setq c-comment-start-regexp "(@|/(/|[*][*]?))")
                (modify-syntax-entry ?@ "< b" java-mode-syntax-table)))

(add-hook 'java-mode-hook (lambda ()
                                (setq c-basic-offset 4
                                      tab-width 4
                                      indent-tabs-mode t)))



(setq save-place-file "~/.emacs.d/saveplace")
(if (version<= emacs-version "25.1")
    (progn
      (setq-default save-place t)
      (require 'saveplace))
  (save-place-mode 1))



                                        ; https://www.reddit.com/r/emacs/comments/idz35e/emacs_27_can_take_svg_screenshots_of_itself/
(defun screenshot-svg ()
  "Save a screenshot of the current frame as an SVG image.
Saves to a temp file and puts the filename in the kill ring."
  (interactive)
  (let* ((filename (make-temp-file "Emacs" nil ".svg"))
         (data (x-export-frames nil 'svg)))
    (with-temp-file filename
      (insert data))
    (kill-new filename)
    (message filename)))


;;; bluer and transparency
( set-frame-parameter nil 'alpha-background 10 )

( add-to-list 'default-frame-alist '( alpha-background . 10 ))

 ;;(set-frame-parameter (selected-frame) 'alpha '(<active> . <inactive>))
 ;;(set-frame-parameter (selected-frame) 'alpha <both>)
 (set-frame-parameter (selected-frame) 'alpha '(85 . 50))
 (add-to-list 'default-frame-alist '(alpha . (85 . 50)))

 (defun toggle-transparency ()
   (interactive)
   (let ((alpha (frame-parameter nil 'alpha)))
     (set-frame-parameter
      nil 'alpha
      (if (eql (cond ((numberp alpha) alpha)
                     ((numberp (cdr alpha)) (cdr alpha))
                     ;; Also handle undocumented (<active> <inactive>) form.
                     ((numberp (cadr alpha)) (cadr alpha)))
               100)
          '(85 . 50) '(100 . 100)))))
 (global-set-key (kbd "C-c t") 'toggle-transparency)

;; Set transparency of emacs
 (defun transparency (value)
   "Sets the transparency of the frame window. 0=transparent/100=opaque"
   (interactive "nTransparency Value 0 - 100 opaque:")
   (set-frame-parameter (selected-frame) 'alpha value))


(lsp-treemacs-sync-mode 1)

(setq display-line-numbers (quote relative))
(global-display-line-numbers-mode t)

(with-eval-after-load 'treemacs
  (define-key treemacs-mode-map [mouse-1] #'treemacs-single-click-expand-action))
(treemacs-load-theme "Default")
(treemacs-git-mode 'deferred)

(setq treemacs-collapse-dirs                   (if treemacs-python-executable 3 0)
      treemacs-deferred-git-apply-delay        0.5
      treemacs-directory-name-transformer      #'identity
      treemacs-display-in-side-window          t
      treemacs-eldoc-display                   'simple
      treemacs-file-event-delay                2000
      treemacs-file-extension-regex            treemacs-last-period-regex-value
      treemacs-file-follow-delay               0.2
      treemacs-file-name-transformer           #'identity
      treemacs-follow-after-init               t
      treemacs-expand-after-init               t
      treemacs-find-workspace-method           'find-for-file-or-pick-first
      treemacs-git-command-pipe                ""
      treemacs-goto-tag-strategy               'refetch-index
      treemacs-header-scroll-indicators        '(nil . "^^^^^^")
      treemacs-hide-dot-git-directory          t
      treemacs-indentation                     2
      treemacs-indentation-string              " "
      treemacs-is-never-other-window           nil
      treemacs-max-git-entries                 5000
      treemacs-missing-project-action          'ask
      treemacs-move-forward-on-expand          nil
      treemacs-no-png-images                   nil
      treemacs-no-delete-other-windows         t
      treemacs-project-follow-cleanup          nil
      treemacs-persist-file                    (expand-file-name ".cache/treemacs-persist" user-emacs-directory)
      treemacs-position                        'left
      treemacs-read-string-input               'from-child-frame
      treemacs-recenter-distance               0.1
      treemacs-recenter-after-file-follow      nil
      treemacs-recenter-after-tag-follow       nil
      treemacs-recenter-after-project-jump     'always
      treemacs-recenter-after-project-expand   'on-distance
      treemacs-litter-directories              '("/node_modules" "/.venv" "/.cask")
      treemacs-project-follow-into-home        nil
      treemacs-show-cursor                     nil
      treemacs-show-hidden-files               t
      treemacs-silent-filewatch                nil
      treemacs-silent-refresh                  nil
      treemacs-sorting                         'alphabetic-asc
      treemacs-select-when-already-in-treemacs 'move-back
      treemacs-space-between-root-nodes        t
      treemacs-tag-follow-cleanup              t
      treemacs-tag-follow-delay                1.5
      treemacs-text-scale                      nil
      treemacs-user-mode-line-format           nil
      treemacs-user-header-line-format         nil
      treemacs-wide-toggle-width               70
      treemacs-width                           35
      treemacs-width-increment                 1
      treemacs-width-is-initially-locked       t
      treemacs-workspace-switch-cleanup        nil
      )


;; The default width and height of the icons is 22 pixels. If you are
;; using a Hi-DPI display, uncomment this to double the icon size.
;;(treemacs-resize-icons 44)

(treemacs-follow-mode t)
(treemacs-filewatch-mode t)
(treemacs-fringe-indicator-mode 'always)
(when treemacs-python-executable
  (treemacs-git-commit-diff-mode t))

(pcase (cons (not (null (executable-find "git")))
             (not (null treemacs-python-executable)))
  (`(t . t)
   (treemacs-git-mode 'deferred))
  (`(t . _)
   (treemacs-git-mode 'simple)))


(treemacs-hide-gitignored-files-mode nil)

(global-set-key (kbd "M-0") 'treemacs-select-window)
(global-set-key (kbd "C-x t 1") 'treemacs-delete-other-windows)
(global-set-key (kbd "C-x t t") 'treemacs)
(global-set-key (kbd "C-x t d") 'treemacs-select-directory)
(global-set-key (kbd "C-x t B") 'treemacs-bookmark)
(global-set-key (kbd "C-x t C-t") 'treemacs-find-file)
(global-set-key (kbd "C-x t M-t") 'treemacs-find-tags)

(treemacs-create-theme "Default"
  :icon-directory (treemacs-join-path treemacs-dir "icons/default")
  :config
  (progn
    (treemacs-create-icon :file "root-open.png"   :fallback ""       :extensions (root-open))
    (treemacs-create-icon :file "root-closed.png" :fallback ""       :extensions (root-closed))
    (treemacs-create-icon :file "emacs.png"       :fallback "🗏 "     :extensions ("el" "elc"))
    (treemacs-create-icon :file "readme.png"      :fallback "🗏 "     :extensions ("readme.md"))
    (treemacs-create-icon :file "src-closed.png"  :fallback " "     :extensions ("src-closed"))
    (treemacs-create-icon :file "src-open.png"    :fallback " "     :extensions ("src-open"))
    (treemacs-create-icon :icon (all-the-icons-icon-for-file "yaml") :extensions ("yml" "yaml"))))

(treemacs-modify-theme "Default"
  :icon-directory "/other/icons/dir"
  :config
  (progn
    (treemacs-create-icon :icon "+" :extensions (dir-closed))
    (treemacs-create-icon :icon "-" :extensions (dir-open))))

(treemacs-define-custom-icon "X " "scripts-closed")
(treemacs-define-custom-icon "Y " "scripts-open")

(treemacs-map-icons-with-auto-mode-alist
 '(".cc" ".hh")
 `((c-mode   . ,(treemacs-get-icon-value "c"))
   (c++-mode . ,(treemacs-get-icon-value "cpp"))))

(with-eval-after-load 'treemacs

  (defun treemacs-ignore-example (filename absolute-path)
    (or (string-equal filename "foo")
        (string-prefix-p "/x/y/z/" absolute-path)))

  (add-to-list 'treemacs-ignored-file-predicates #'treemacs-ignore-example))



(defun flyspell-on-for-buffer-type ()
  "Enable Flyspell appropriately for the major mode of the current buffer.  Uses `flyspell-prog-mode' for modes derived from `prog-mode', so only strings and comments get checked.  All other buffers get `flyspell-mode' to check all text.  If flyspell is already enabled, does nothing."
  (interactive)
  (if (not (symbol-value flyspell-mode))  ; if not already on
	  (progn
	    (if (derived-mode-p 'prog-mode)
	        (progn
	          (message "Flyspell on (code)")
	          (flyspell-prog-mode))
	      ;; else
	      (progn
	        (message "Flyspell on (text)")
	        (flyspell-mode 1)))
	    ;; I tried putting (flyspell-buffer) here but it didn't seem to work
	    )))

(defun flyspell-toggle ()
  "Turn Flyspell on if it is off, or off if it is on.  When turning on, it uses `flyspell-on-for-buffer-type' so code-vs-text is handled appropriately."
  (interactive)
  (if (symbol-value flyspell-mode)
	  (progn ; flyspell is on, turn it off
	    (message "Flyspell off")
	    (flyspell-mode -1))
                                        ; else - flyspell is off, turn it on
	(flyspell-on-for-buffer-type)))

(add-hook 'find-file-hook 'flyspell-on-for-buffer-type)
(add-hook 'after-change-major-mode-hook 'flyspell-on-for-buffer-type)


;; Make parens less visible
(font-lock-add-keywords 'scheme48-mode '(("[()]" . 'paren-face)))
(defface paren-face
  '((t (:foreground "gray60")))
  "The face used for parenthesises.")


;; Remind me to take typing breaks
(setq type-break-mode-line-message-mode t
      type-break-demo-functions '(type-break-demo-boring)
      type-break-time-warning-intervals '()
      ;; No type break file
      type-break-file-name nil)
(type-break-mode)

(defun undo-tree-visualizer-update-linum (&rest args)
  (linum-update undo-tree-visualizer-parent-buffer))
(advice-add 'undo-tree-visualize-undo :after #'undo-tree-visualizer-update-linum)
(advice-add 'undo-tree-visualize-redo :after #'undo-tree-visualizer-update-linum)
(advice-add 'undo-tree-visualize-undo-to-x :after #'undo-tree-visualizer-update-linum)
(advice-add 'undo-tree-visualize-redo-to-x :after #'undo-tree-visualizer-update-linum)
(advice-add 'undo-tree-visualizer-mouse-set :after #'undo-tree-visualizer-update-linum)
(advice-add 'undo-tree-visualizer-set :after #'undo-tree-visualizer-update-linum)


;; …every time Flycheck is activated in a new buffer
(add-hook 'flycheck-mode-hook #'my/set-flycheck-margins)

(add-hook 'after-init-hook #'global-flycheck-mode)

;;; Goto line file
(defadvice find-file (around find-file-line-number
                             (filename &optional wildcards)
                             activate)
  "Turn files like file.cpp:14 into file.cpp and going to the 14-th line."
  (save-match-data
    (let* ((matched (string-match "^\\(.*\\):\\([0-9]+\\):?$" filename))
           (line-number (and matched
                           (match-string 2 filename)
                           (string-to-number (match-string 2 filename))))
           (filename (if matched (match-string 1 filename) filename)))
      ad-do-it
      (when line-number
        ;; goto-line is for interactive use
        (goto-char (point-min))
        (forward-line (1- line-number))))))


(use-package flycheck-posframe
  :if (version<= "26.1" emacs-version)
  :after flycheck
  :config
  (add-hook 'flycheck-mode-hook #'flycheck-posframe-mode)
  (flycheck-posframe-configure-pretty-defaults))

(use-package flyspell
  :defer t
  :diminish flyspell-mode
  :init
  (defhydra hydra-spelling ()
    ("b" flyspell-buffer "check buffer")
    ("d" ispell-change-dictionary "change dictionary")
    ("n" flyspell-goto-next-error "next")
    ("c" flyspell-correct-previous-word-generic "correct")
    ("q" nil "quit"))
  :config
  (progn
    (add-hook 'prog-mode-hook #'flyspell-prog-mode)
    (add-hook 'text-mode-hook #'turn-on-flyspell)
    (add-hook 'org-mode-hook #'turn-on-flyspell)))


(eval-after-load 'flycheck
  '(flycheck-package-setup))

(add-hook 'haskell-mode-hook #'flycheck-haskell-setup)

(with-eval-after-load 'rust-mode
  (add-hook 'flycheck-mode-hook #'flycheck-rust-setup))

;; Check 'echo -n' usage
(setq flycheck-checkbashisms-newline t)

;; Check non-POSIX issues but required to be supported  by Debian Policy 10.4
;; Setting this variable to non nil made flycheck-checkbashisms-newline effects
;; regardless of its value
(setq flycheck-checkbashisms-posix t)


(use-package flyspell-correct
  :commands (flyspell-correct-wrapper))

(use-package flycheck
  :defer t
  :init
  :config
  (setq flycheck-display-errors-delay 0.2)
  (progn
    ;; Custom fringe indicator
    (when (and (fboundp 'define-fringe-bitmap)
		       ;; (not syntax-checking-use-original-bitmaps)
		       )
	  (define-fringe-bitmap 'my-flycheck-fringe-indicator
	    (vector #b00000000
		        #b00000000
		        #b00000000
		        #b00000000
		        #b00000000
		        #b00000000
		        #b00000000
		        #b00011100
		        #b00111110
		        #b00111110
		        #b00111110
		        #b00011100
		        #b00000000
		        #b00000000
		        #b00000000
		        #b00000000
		        #b00000000)))
    (let ((bitmap 'my-flycheck-fringe-indicator
		          ;; (if syntax-checking-use-original-bitmaps
		          ;;            'flycheck-fringe-bitmap-double-arrow
		          ;;   'my-flycheck-fringe-indicator)
		          ))
	  (flycheck-define-error-level 'error
	    :severity 2
	    :overlay-category 'flycheck-error-overlay
	    :fringe-bitmap bitmap
	    :error-list-face 'flycheck-error-list-error
	    :fringe-face 'flycheck-fringe-error)
	  (flycheck-define-error-level 'warning
	    :severity 1
	    :overlay-category 'flycheck-warning-overlay
	    :fringe-bitmap bitmap
	    :error-list-face 'flycheck-error-list-warning
	    :fringe-face 'flycheck-fringe-warning)
	  (flycheck-define-error-level 'info
	    :severity 0
	    :overlay-category 'flycheck-info-overlay
	    :fringe-bitmap bitmap
	    :error-list-face 'flycheck-error-list-info
	    :fringe-face 'flycheck-fringe-info))))



(add-to-list 'display-buffer-alist
             `(,(rx bos "*Flycheck errors*" eos)
               (display-buffer-reuse-window
                display-buffer-in-side-window)
               (side            . bottom)
               (reusable-frames . visible)
               (window-height   . 1.0)))

(setq flycheck-display-errors-function
      #'flycheck-display-error-messages-unless-error-list)

( global-flycheck-mode )
( add-hook 'after-init-hook #'global-flycheck-mode )
(setq flycheck-check-syntax-automatically '(mode-enabled save))
;; Show indicators in the left margin
(setq flycheck-indication-mode 'nil)


;; Adjust margins and fringe widths…
(defun my/set-flycheck-margins ()
  (setq left-fringe-width 8 right-fringe-width 8
        left-margin-width 1 right-margin-width 0)
  (flycheck-refresh-fringes-and-margins))

;; Enable flycheck-mode to display flycheck diagnostics
(add-hook 'go-mode-hook 'flycheck-mode)
(add-hook 'python-mode-hook 'flycheck-mode)
(add-hook 'java-mode-hook 'flycheck-mode)
(add-hook 'c-mode-hook 'flycheck-mode)

;;( add-hook 'c-mode-hook 'lsp )

( use-package company-flx )
( with-eval-after-load 'company
  ( company-flx-mode +1 ))

( use-package company )
( add-hook 'after-init-hook  'global-company-mode )
( setq company-format-margin-function #'company-dot-icons-margin )

( defun company-dot-icons-margin ( candidate selected )
  "..."
  ( propertize "" 'face
    ( cl-case ( company-call-backend 'kind candidate )
      ( 'class 'font-lock-type-face )
      ( 'variable 'font-lock-variable-name-face )
      ( 'method 'font-lock-function-name-face )
                  ;;; ...
      ( _ 'default ))))

( setq company-tooltip-align-annotations 1 )
;;; (global-set-key ( kbd "<tab>" )
( lambda ( )
  ( interactive )
  ( let ((company-tooltip-idle-delay 0.0 ))
    ( company-complete )
    ( and company-candidates
      ( company-call-frontends 'post-command ))))

( use-package company-box )
( add-hook 'company-mode  'company-box-mode )

(use-package company
  :diminish company-mode
  :init
  (progn
    (add-hook 'after-init-hook 'global-company-mode))
  :config
  (progn
    (let ((map company-active-map))
	  (define-key map (kbd "C-j") 'company-select-next)
	  (define-key map (kbd "C-k") 'company-select-previous)
	  (define-key map (kbd "C-l") 'company-complete-selection))
    (setq company-idle-delay 0
	      company-show-numbers t
	      company-tooltip-idle-delay 0
	      company-minimum-prefix-length 2
	      company-dabbrev-ignore-case nil
	      company-dabbrev-downcase nil
	      company-ispell-dictionary (file-truename "~/.emacs.d/dict/english-words.txt")
	      ispell-alternate-dictionary (file-truename "~/.emacs.d/dict/english-words.txt")
	      company-transformers '(company-sort-by-backend-importance)
	      company-dabbrev-code-other-buffers 'code
	      company-dabbrev-ignore-case nil
	      company-dabbrev-downcase nil
	      company-dabbrev-code-time-limit 5

        ;;; add completion code mode
        company-dabbrev-code-modes '(c-mode inferior-python-mode)
        company-dabbrev-code-modes '(java-mode inferior-python-mode)
	      company-dabbrev-code-modes '(python-mode inferior-python-mode)
	      company-backends '(company-capf
			                 company-dabbrev-code
			                 company-keywords
			                 company-files
			                 company-ispell
			                 company-yasnippet
			                 company-abbrev
			                 company-dabbrev))
    (defun ora-company-number ()
	  "Forward to `company-complete-number'.
 Unless the number is potentially part of the candidate.
 In that case, insert the number."
	  (interactive)
	  (let* ((k (this-command-keys))
	         (re (concat "^" company-prefix k)))
	    (if (cl-find-if (lambda (s) (string-match re s))
			            company-candidates)
	        (self-insert-command 1)
	      (company-complete-number (string-to-number k)))))
    (let ((map company-active-map))
	  (mapc
	   (lambda (x)
	     (define-key map (format "%d" x) 'ora-company-number
	       ))
	   (number-sequence 0 9))
	  (define-key map " " (lambda ()
			                (interactive)
			                (company-abort)
			                (self-insert-command 1)))
	  (define-key map (kbd "<return>") nil))))

(use-package company-statistics
  :after company
  :config
  (company-statistics-mode))


(add-hook 'dap-stopped-hook
          (lambda (arg) (call-interactively #'dap-hydra)))

;; Enabling only some features
(setq dap-auto-configure-features '(sessions locals controls tooltip))
(dap-mode 1)

;; The modes below are optional

(dap-ui-mode 1)
;; enables mouse hover support
(dap-tooltip-mode 1)
;; use tooltips for mouse hover
;; if it is not enabled `dap-mode' will use the minibuffer.
(tooltip-mode 1)
;; displays floating panel with debug buttons
;; requies emacs 26+
(dap-ui-controls-mode 1)


;; -*- lexical-binding: t -*-
(define-minor-mode +dap-running-session-mode
  "A mode for adding keybindings to running sessions"
  nil
  nil
  (make-sparse-keymap)
  (evil-normalize-keymaps) ;; if you use evil, this is necessary to update the keymaps
  ;; The following code adds to the dap-terminated-hook
  ;; so that this minor mode will be deactivated when the debugger finishes
  (when +dap-running-session-mode
    (let ((session-at-creation (dap--cur-active-session-or-die)))
      (add-hook 'dap-terminated-hook
                (lambda (session)
                  (when (eq session session-at-creation)
                    (+dap-running-session-mode -1)))))))

;; Activate this minor mode when dap is initialized
(add-hook 'dap-session-created-hook '+dap-running-session-mode)

;; Activate this minor mode when hitting a breakpoint in another file
(add-hook 'dap-stopped-hook '+dap-running-session-mode)

;; Activate this minor mode when stepping into code in another file
(add-hook 'dap-stack-frame-changed-hook (lambda (session)
                                          (when (dap--session-running session)
                                            (+dap-running-session-mode 1))))




;;; Dap java
(dap-register-debug-template "My Runner"
                             (list :type "java"
                                   :request "launch"
                                   :args ""
                                   :vmArgs "-ea -Dmyapp.instance.name=myapp_1"
                                   :projectName "myapp"
                                   :mainClass "com.domain.AppRunner"
                                   :env '(("DEV" . "1"))))


(dap-register-debug-provider
 "programming-language-name"
 (lambda (conf)
   (plist-put conf :debugPort 1234)
   (plist-put conf :host "localhost")
   conf))

(dap-register-debug-template "Example Configuration"
                             (list :type "java"
                                   :request "launch"
                                   :args ""
                                   :name "Run Configuration"))




;;; Dap kotlin
(dap-register-debug-template "Kotlin tests with launcher"
                             (list :type "kotlin"
                                   :request "launch"
                                   :mainClass "org.junit.platform.console.ConsoleLauncher --scan-class-path"
                                   :enableJsonLogging nil
                                   :noDebug nil))

;;; dap py -> pip install "debugpy"
(require 'dap-python)
;; if you installed debugpy, you need to set this
;; https://github.com/emacs-lsp/dap-mode/issues/306
(setq dap-python-debugger 'debugpy)


(dap-register-debug-template "My App"
                             (list :type "python"
                                   :args "-i"
                                   :cwd nil
                                   :env '(("DEBUG" . "1"))
                                   :target-module (expand-file-name "~/src/myapp/.env/bin/myapp")
                                   :request "launch"
                                   :name "My App"))



;;; dap rust
(dap-register-debug-template "Rust::GDB Run Configuration"
                             (list :type "gdb"
                                   :request "launch"
                                   :name "GDB::Run"
                                   :gdbpath "rust-gdb"
                                   :target nil
                                   :cwd nil))

;;; Dap dart

(when (cl-find-if-not #'package-installed-p package-selected-packages)
  (package-refresh-contents)
  (mapc #'package-install package-selected-packages))

(add-hook 'dart-mode-hook 'lsp)

(setq gc-cons-threshold (* 100 1024 1024)
      read-process-output-max (* 1024 1024))


(dap-register-debug-template "Flutter :: Custom debug"
                             (list :flutterPlatform "x86_64"
                                   :program "lib/main_debug.dart"
                                   :args '("--flavor" "customer_a")))


;;; Hover developer mobile
(use-package dart-mode
  :custom
  (dart-sdk-path (concat (getenv "HOME") "/flutter/bin/cache/dark-sdk/")
                 dart-format-on-save t))

(use-package hover
  :after dart-mode)


(global-set-key (kbd "C-M-p") 'hover-take-screenshot)
(global-set-key (kbd "C-M-z") 'hover-run-or-hot-reload)
(global-set-key (kbd "C-M-x") 'hover-run-or-hot-restart)

(setq hover-flutter-sdk-path (concat (getenv "HOME") "/flutter") ; remove if `flutter` is already in $PATH
      hover-command-path (concat (getenv "GOPATH") "/bin/hover") ; remove if `hover` is already in $PATH
      hover-hot-reload-on-save t
      hover-screenshot-path (concat (getenv "HOME") "/Pictures")
      hover-screenshot-prefix "my-prefix-"
      hover-observatory-uri "http://my-custom-host:50300"
      hover-clear-buffer-on-hot-restart t)
(hover-minor-mode 1)


(use-package lsp-ui
  :commands lsp-ui-mode
  :config
  (setq lsp-ui-doc-enable nil)
  (setq lsp-ui-doc-header t)
  (setq lsp-ui-doc-include-signature t)
  (setq lsp-ui-doc-border (face-foreground 'default))
  (setq lsp-ui-sideline-show-code-actions t)
  (setq lsp-ui-sideline-delay 0.05))

(use-package lsp-mode
  :hook ((c-mode          ; clangd
          c++-mode        ; clangd
          c-or-c++-mode   ; clangd
          java-mode       ; eclipse-jdtls
          js-mode         ; ts-ls (tsserver wrapper)
          js-jsx-mode     ; ts-ls (tsserver wrapper)
          typescript-mode ; ts-ls (tsserver wrapper)
          python-mode     ; pyright
          web-mode        ; ts-ls/HTML/CSS
          haskell-mode    ; haskell-language-server
          ) . lsp-deferred)
  :commands lsp
  :config
  (setq lsp-auto-guess-root t)
  (setq lsp-log-io nil)
  (setq lsp-restart 'auto-restart)
  (setq lsp-enable-symbol-highlighting nil)
  (setq lsp-enable-on-type-formatting nil)
  (setq lsp-signature-auto-activate nil)
  (setq lsp-signature-render-documentation nil)
  (setq lsp-eldoc-hook nil)
  (setq lsp-modeline-code-actions-enable nil)
  (setq lsp-modeline-diagnostics-enable nil)
  (setq lsp-headerline-breadcrumb-enable nil)
  (setq lsp-semantic-tokens-enable nil)
  (setq lsp-enable-folding nil)
  (setq lsp-enable-imenu nil)
  (setq lsp-enable-snippet nil)
  (setq read-process-output-max (* 1024 1024)) ;; 1MB
  (setq lsp-idle-delay 0.5))

(with-eval-after-load 'lsp-mode
  ;; :global/:workspace/:file
  (setq lsp-modeline-diagnostics-scope :workspace))


(use-package lsp-mode
  :commands lsp
  :diminish lsp-mode
  :commands lsp
  :hook (python-mode)
  :config
  (progn
    (require 'lsp-mode)
    (setq lsp-message-project-root-warning t
	      lsp-prefer-flymake nil
	      lsp-restart 'ignore))
  (lsp))

(use-package company-lsp
  :after (company lsp-mode)
  :init
  (progn
    (add-hook 'python-mode-hook
		      (lambda ()
		        (add-to-list (make-local-variable 'company-backends) '(company-lsp company-dabbrev-code company-files)))))
  :config
  (progn
    (setq company-lsp-enable-recompletion t
	      company-lsp-async t
	      company-lsp-cache-candidates nil)))



(with-eval-after-load 'projectile
  (add-to-list 'projectile-project-root-files-bottom-up "pubspec.yaml")
  (add-to-list 'projectile-project-root-files-bottom-up "BUILD"))



;; Like looking-at but with an offset
(defun my-looking-at (regexp &optional offset)
  (let ((pos (+ (or offset 0) (point))))
    (when (and (>= pos (point-min)) (< pos (point-max)))
      (string-match regexp (string (char-after pos))))))

;; Call the right forward function and move past otherchars
(defun my-forward (&optional backward)
  (let ((ispell-casechars (ispell-get-casechars))
        (ispell-otherchars (ispell-get-otherchars))
        (ispell-many-otherchars-p (ispell-get-many-otherchars-p))
        (offset (if backward -1 0))
        (dir (if backward -1 1))
        (continue t))
    (if subword-mode (subword-forward dir) (forward-word dir))
    (while (and continue
                (not (string= "" ispell-otherchars))
                (my-looking-at ispell-otherchars offset)
                (my-looking-at ispell-casechars (+ dir offset)))
      (if subword-mode (subword-forward dir) (forward-word dir))
      (setq continue ispell-many-otherchars-p))))
(defun my-backward () (my-forward t))

;; Properly find boundaries of words in CamelCase
(defun my-ispell-get-word (orig-fun &optional following extra-otherchars)
  (if (not subword-mode)
      (funcall orig-fun following extra-otherchars)
    (if following (my-forward) (if (not (eobp)) (forward-char)))
    (let* ((beg (progn (my-backward) (point-marker)))
           (end (progn (my-forward) (point-marker)))
           (word (buffer-substring-no-properties beg end)))
      (list word beg end))))
(advice-add #'ispell-get-word :around #'my-ispell-get-word)
(advice-add #'flyspell-get-word :around #'my-ispell-get-word)

;; Simplify and use my-forward to handle CamelCase words
(defun my-flyspell-small-region (orig-fun beg end)
  ;; (if (not subword-mode)
  ;;     (funcall orig-fun beg end)
  (save-excursion
    (if (> beg end) (setq beg (prog1 end (setq end beg))))
    (if (< beg (point-min)) (setq beg (point-min)))
    (if (> end (point-max)) (setq end (point-max)))
    (goto-char beg)
    (while (< (point) end)
      (flyspell-word t)
      ;; (sit-for 0) ;; uncomment to enable animation
      (my-forward))))
(advice-add #'flyspell-small-region :around #'my-flyspell-small-region)

;; Fake handling of CamelCase words in flyspell-large-region
(defun my-flyspell-large-region (orig-fun beg end)
  (let ((ispell-extra-args ispell-extra-args)
        (subword-mode subword-mode))
    (when (and subword-mode (string-match "aspell\\'" ispell-program-name))
      (push "--run-together" ispell-extra-args)
      (push "--run-together-limit=6" ispell-extra-args)
      (push "--run-together-min=2" ispell-extra-args)
      (setq subword-mode nil))
    (funcall orig-fun beg end)))
(advice-add #'flyspell-large-region :around #'my-flyspell-large-region)

;; Use aspell if installed
(when (executable-find "aspell")
  (setq ispell-program-name "aspell")
  (setq ispell-list-command "--list"))

;; Only check the previous word if no longer editing it
(defun my-flyspell-check-pre-word-p ()
  (and (eq flyspell-pre-buffer (current-buffer))
       (numberp flyspell-pre-point)
       (/= (save-excursion
             (goto-char (1- flyspell-pre-point))
             (my-forward)
             (point))
           (save-excursion
             (if (not (bobp)) (backward-char))
             (my-forward)
             (point)))))

;; Simplify and use flyspell-region in the post-command-hook
(defun my-flyspell-post-command-hook (orig-fun)
  (when flyspell-mode
    (with-local-quit
      (let ((command this-command)
            deactivate-mark)
        ;; Handle word at previous location
        (when (my-flyspell-check-pre-word-p)
          (save-excursion
            (goto-char (1- flyspell-pre-point))
            (flyspell-word)))
        ;; Handle word at current location
        (when (flyspell-check-word-p)
          (flyspell-word))
        ;; Handle all other recent changes
        (while (and (not (input-pending-p)) (consp flyspell-changes))
          (let* ((change (pop flyspell-changes))
                 (beg (car change))
                 (end (cdr change)))
            (flyspell-region beg end)))
        (setq flyspell-previous-command command)))))
(advice-add #'flyspell-post-command-hook :around #'my-flyspell-post-command-hook)


(use-package parrot
  :config
  (parrot-mode))

(setq parrot-rotate-dict
      '(
        (:rot ("alpha" "beta") :caps t :lower nil)
        ;; => rotations are "Alpha" "Beta"

        (:rot ("snek" "snake" "stawp"))
        ;; => rotations are "snek" "snake" "stawp"

        (:rot ("yes" "no") :caps t :upcase t)
        ;; => rotations are "yes" "no", "Yes" "No", "YES" "NO"

        (:rot ("&" "|"))
        ;; => rotations are "&" "|"

        ;; default dictionary starts here ('v')
        (:rot ("begin" "end") :caps t :upcase t)
        (:rot ("enable" "disable") :caps t :upcase t)
        (:rot ("enter" "exit") :caps t :upcase t)
        (:rot ("forward" "backward") :caps t :upcase t)
        (:rot ("front" "rear" "back") :caps t :upcase t)
        (:rot ("get" "set") :caps t :upcase t)
        (:rot ("high" "low") :caps t :upcase t)
        (:rot ("in" "out") :caps t :upcase t)
        (:rot ("left" "right") :caps t :upcase t)
        (:rot ("min" "max") :caps t :upcase t)
        (:rot ("on" "off") :caps t :upcase t)
        (:rot ("prev" "next"))
        (:rot ("start" "stop") :caps t :upcase t)
        (:rot ("true" "false") :caps t :upcase t)
        (:rot ("&&" "||"))
        (:rot ("==" "!="))
        (:rot ("." "->"))
        (:rot ("if" "else" "elif"))
        (:rot ("ifdef" "ifndef"))
        (:rot ("int8_t" "int16_t" "int32_t" "int64_t"))
        (:rot ("uint8_t" "uint16_t" "uint32_t" "uint64_t"))
        (:rot ("1" "2" "3" "4" "5" "6" "7" "8" "9" "10"))
        (:rot ("1st" "2nd" "3rd" "4th" "5th" "6th" "7th" "8th" "9th" "10th"))
        ))

(parrot-set-parrot-type 'emacs)
(parrot-start-animation)
(setq parrot-num-rotations nil)



;;; Hooks
( autoload 'paredit-mode "paredit" t )
( add-hook 'emacs-lisp-mode-hook       ( lambda () (paredit-mode +1 )))
( add-hook 'lisp-mode-hook             ( lambda ( ) (paredit-mode +1 )))
( add-hook 'lisp-interaction-mode-hook ( lambda ( ) ( paredit-mode +1 )))
( add-hook 'scheme-mode-hook ( lambda ( ) ( paredit-mode +1 )))


( custom-set-faces
;;; custom-set-faces was added by Custom.
;;; If you edit it by hand, you could mess it up, so be careful.
;;; Your init file should contain only one such instance.
;;; If there is more than one, they won't work right.
  '( fixed-pitch (( t ( :family "Iosevka" :height 150 ))))
  '( hi-yellow (( t  ( :foreground "#00ff00" ))))
  '( shadow (( t ( :foreground "#00ff00" ))))
  '( variable-pitch (( t ( :family "Sans Serif" :height 170 )))))
  ;;; '( focus-unfocused (( t ( :background "#cccccc" )))))


( toggle-cursor-type-when-idle 1 ) ;;; Turn on cursor change when Emacs is idle
( change-cursor-mode 1)  ;;; Turn on change for overwrite, read-only, and input mode

( add-hook 'foo-mode-hook ( lambda ( ) ( interactive ) ( column-marker-1 80 )))
( column-marker-create column-marker-4 my-favorite-face )


( add-hook 'lisp-mode-hook 'highlight-sexp-mode )
( add-hook 'emacs-lisp-mode-hook 'highlight-sexp-mode )
  ;;; editar depois
  ;;; The following variables are available for customization:
  ;;; hl-sexp-background-color
  ;;; hl-sexp-foreground-color
  ;;; hl-sexp-face

( when ( file-exists-p ( concat ( getenv "XDG_RUNTIME_DIR" ) "/tk.sock" ))
  ( load-file ( concat ( getenv "HOME" ) ".emacs.d/plugins/interface/tkenter-mode.el" ))
  ( ensc/tk-start ))

;;; (add-hook 'after-init-hook 'global-color-identifiers-mode )
( setq color-identifiers:recoloring-delay 1 )


( beacon-mode 1 )
( setq beacon-color "#AA8768" ) ;;; Color becon
( setq beacon-blink-when-window-scrolls t )
( setq beacon-blink-when-point-moves t )
( setq beacon-blink-when-window-changes t )


( add-hook 'after-change-major-mode-hook ( lambda ( ) ( add-to-invisibility-spec 'hl )))
( defadvice hide-lines-matching ( around hide-line-clear activate )
  ( if ( equal search-text "" )
      ( hide-lines-show-all )
    ad-do-it ))



( gcmh-mode 1 )


(when (executable-find "curl")
  (setq helm-google-suggest-use-curl-p t))

(setq helm-split-window-in-side-p           t ; open helm buffer inside current window, not occupy whole other window
      helm-move-to-line-cycle-in-source     t ; move to end or beginning of source when reaching top or bottom of source.
      helm-ff-search-library-in-sexp        t ; search for library in `require' and `declare-function' sexp.
      helm-scroll-amount                    8 ; scroll 8 lines other window using M-<next>/M-<prior>
      helm-ff-file-name-history-use-recentf t
      helm-echo-input-in-header-line t)

(defun spacemacs//helm-hide-minibuffer-maybe ()
  "Hide minibuffer in Helm session if we use the header line as input field."
  (when (with-helm-buffer helm-echo-input-in-header-line)
    (let ((ov (make-overlay (point-min) (point-max) nil nil t)))
      (overlay-put ov 'window (selected-window))
      (overlay-put ov 'face
                   (let ((bg-color (face-background 'default nil)))
                     `(:background ,bg-color :foreground ,bg-color)))
      (setq-local cursor-type nil))))


(add-hook 'helm-minibuffer-set-up-hook
          'spacemacs//helm-hide-minibuffer-maybe)

(setq helm-autoresize-max-height 0)
(setq helm-autoresize-min-height 20)
(helm-autoresize-mode 1)

( helm-mode 1 )
(helm-descbinds-mode)
(setq wgrep-auto-save-buffer t)
(setq wgrep-enable-key "r")
(setq wgrep-change-readonly-file t)

;; Save buffer when helm-multi-swoop-edit complete
(setq helm-multi-swoop-edit-save t)

;; If this value is t, split window inside the current window
(setq helm-swoop-split-with-multiple-windows nil)

;; Split direcion. 'split-window-vertically or 'split-window-horizontally
(setq helm-swoop-split-direction 'split-window-horizontally)

;; If nil, you can slightly boost invoke speed in exchange for text color
(setq helm-swoop-speed-or-color nil)

;; ;; Go to the opposite side of line from the end or beginning of line
(setq helm-swoop-move-to-line-cycle t)

;; Optional face for line numbers
;; Face name is `helm-swoop-line-number-face`
;;(setq helm-swoop-use-line-number-face t)

;; If you prefer fuzzy matching
(setq helm-swoop-use-fuzzy-match t)

;; If you would like to use migemo, enable helm's migemo feature
;; (helm-migemo-mode 1)



;; Use search query at the cursor  (default)
(setq helm-swoop-pre-input-function
      (lambda () (thing-at-point 'symbol)))

;; Disable pre-input
(setq helm-swoop-pre-input-function
      (lambda () ""))
;; Or, just use M-x helm-swoop-without-pre-input

;; Match only for symbol
(setq helm-swoop-pre-input-function
      (lambda () (format "\\_<%s\\_> " (thing-at-point 'symbol))))

;; Always use the previous search for helm. Remember C-<backspace> will delete entire line
(setq helm-swoop-pre-input-function
      (lambda () (if (boundp 'helm-swoop-pattern)
                     helm-swoop-pattern "")))

;; If there is no symbol at the cursor, use the last used words instead.
(setq helm-swoop-pre-input-function
      (lambda ()
        (let (($pre-input (thing-at-point 'symbol)))
          (if (eq (length $pre-input) 0)
              helm-swoop-pattern ;; this variable keeps the last used words
            $pre-input))))

;; If a symbol or phrase is selected, use it as the initial query.
(setq helm-swoop-pre-input-function
      (lambda ()
        (if mark-active
            (buffer-substring-no-properties (mark) (point))
          "")))



;; The default "C-x c" is quite close to "C-x C-c", which quits Emacs.
;; Changed to "C-c h". Note: We must set "C-c h" globally, because we
;; cannot change `helm-command-prefix-key' once `helm-config' is loaded.
(global-set-key (kbd "C-c h") 'helm-command-prefix)
(global-unset-key (kbd "C-x c"))

(define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action) ; rebind tab to run persistent action
(define-key helm-map (kbd "C-i") 'helm-execute-persistent-action) ; make TAB work in terminal
(define-key helm-map (kbd "C-z")  'helm-select-action) ; list actions using C-z

(when (executable-find "curl")
  (setq helm-google-suggest-use-curl-p t))

(setq helm-split-window-in-side-p           t ; open helm buffer inside current window, not occupy whole other window
      helm-move-to-line-cycle-in-source     t ; move to end or beginning of source when reaching top or bottom of source.
      helm-ff-search-library-in-sexp        t ; search for library in `require' and `declare-function' sexp.
      helm-scroll-amount                    8 ; scroll 8 lines other window using M-<next>/M-<prior>
      helm-ff-file-name-history-use-recentf t
      helm-echo-input-in-header-line t)

(defun spacemacs//helm-hide-minibuffer-maybe ()
  "Hide minibuffer in Helm session if we use the header line as input field."
  (when (with-helm-buffer helm-echo-input-in-header-line)
    (let ((ov (make-overlay (point-min) (point-max) nil nil t)))
      (overlay-put ov 'window (selected-window))
      (overlay-put ov 'face
                   (let ((bg-color (face-background 'default nil)))
                     `(:background ,bg-color :foreground ,bg-color)))
      (setq-local cursor-type nil))))


(add-hook 'helm-minibuffer-set-up-hook
          'spacemacs//helm-hide-minibuffer-maybe)

(setq helm-autoresize-max-height 0)
(setq helm-autoresize-min-height 20)
(helm-autoresize-mode 1)

;;; (add-hook 'prog-mode-hook 'format-all-mode)
(add-hook 'format-all-mode-hook 'format-all-ensure-formatter)

( global-undo-tree-mode )
( setq undo-tree-show-minibuffer-help t )
( setq undo-tree-show-help-in-visualize-buffer t )
( setq undo-tree-minibuffer-help-dynamic t )

;; Always start smartparens mode in js-mode.
(add-hook 'js-mode-hook #'smartparens-mode)

(add-hook 'c-mode-hook #'smartparens-mode)


(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(when (file-exists-p custom-file)
  (load custom-file 'noerror))

(defun renz/display-ansi-colors ()
  "Render colors in a buffer that contains ASCII color escape codes."
  (interactive)
  (require 'ansi-color)
  (let ((inhibit-read-only t))
    (ansi-color-apply-on-region (point-min) (point-max))))

(defun renz/modify-margins ()
  "Add some space around each window."
  (modify-all-frames-parameters
   '((right-divider-width . 3)
     (internal-border-width . 3)))
  (dolist (face '(window-divider
                  window-divider-first-pixel
                  window-divider-last-pixel))
    (face-spec-reset-face face)
    (set-face-foreground face (face-attribute 'default :background)))
  (set-face-background 'fringe (face-attribute 'default :background)))

(renz/modify-margins)

(add-hook 'ef-themes-post-load-hook 'renz/modify-margins)

;;; Settings System


( transient-mark-mode 1 )
( column-number-mode )
( global-display-line-numbers-mode 0 )

(if (version<= "27.1" emacs-version)
    (global-so-long-mode 1))


( put 'narrow-to-defun  'disabled nil )
( put 'narrow-to-page   'disabled nil )
( put 'narrow-to-region 'disabled nil )

;; Show me your parens!
( setq show-paren-delay 0
  show-paren-style 'expression )
( show-paren-mode 1 )

( setq c-default-style "linux"
  c-basic-offset 4 )

( setq whitespace-style '( lines ))
( setq whitespace-line-column 78 )
( global-whitespace-mode 1 )
( setq whitespace-style '( tabs trailing lines tab-mark ))

( font-lock-add-keywords
  'emacs-lisp-mode
  '(( "^[^\n]\\{80\\}\\(.*\\)$"
      1 font-lock-warning-face prepend )))

( setq-default indicate-empty-lines t )
    ;;; Hide the comments too when you do a 'hs-hide-all'
( setq hs-hide-comments nil )
    ;;; Set whether isearch opens folded comments, code, or both
    ;;; where x is code, comments, t ( both ), or nil ( neither )
( setq hs-isearch-open 'x )
    ;;; Add more here
( setq mark-even-if-inactive t )
( setq vc-make-backup-files t )
( setq inhibit-startup-message t )
( setq visible-bell t )
( setq ring-bell-function 'ignore )
( setq auto-save-timeout 60 )
( setq make-backup-files nil )
( setq blink-matching-paren-distance nil )
( setq pop-up-frames t )
( setq debug-on-error t )
( setq backup-by-copying-when-mismatch t )
( setq require-final-newline 't )
( setq special-display-buffer-names
  ( nconc '( "*Backtrace*" "*VC-log*" "*compilation*" "*grep*" )
  	special-display-buffer-names ))

( add-to-list 'special-display-frame-alist '( tool-bar-lines . 0 ))

( setq recentf-max-menu-items 25 )
( setq recentf-max-saved-items 25 )
( run-at-time nil ( * 5 60 ) 'recentf-save-list )

;;; Make startup faster by reducing the frequency of garbage
;;; collection.  The default is 800 kilobytes.  Measured in bytes.
( setq gc-cons-threshold ( * 50 1000 1000 ))
;;; The rest of the init file.
;;; Make gc pauses faster by decreasing the threshold.
( setq gc-cons-threshold ( * 2 1000 1000 ))

;;; Make backup files even when in version controlled directory.
( setq vc-make-backup-files t )

;;; Number backup files.
( setq version-control t )

;;; Double space for sentences.
( setq-default sentence-end-double-space t )

;;; Store pastes from other programs in the kill-ring before
;;; overwriting with Emacs' yanks.
( setq save-interprogram-paste-before-kill t )

;;; Newline at end of file.
( setq require-final-newline t )

( defalias 'yes-or-no-p 'y-or-n-p )

( cua-mode t )
( random t )
( global-font-lock-mode 1 )
( auto-compression-mode 1 )
( menu-bar-mode -1 )
( tool-bar-mode -1 )
( tooltip-mode -1 )
( scroll-bar-mode -1 )
( set-fringe-mode 10 )
( blink-cursor-mode 1 )
( winner-mode 1 )
( which-function-mode 1 )
;;; ( desktop-save-mode 1 )
;; ( mouse-sel-mode 1 )
( global-ligature-mode t )
;;; desviar o cursor do rato do ponto de escrita
( mouse-avoidance-mode 'jump )

;;; Highlight the marked region by C-<space>
( transient-mark-mode 1 )
;;; Turn on the global-font-lock-mode
( global-font-lock-mode t )

( setq font-lock-maximum-decoration t ) ;;; muitas decorações

( setq sentence-end-double-space nil )

(custom-set-variables

;;; custom-set-variables was added by Custom.
;;; If you edit it by hand, you could mess it up, so be careful.
;;; Your init file should contain only one such instance.
;;; If there is more than one, they won't work right.

 '( c-default-style "k&r" )   ;;; estilo de indentação C
 '( fill-column 80 )          ;;; número de colunas
 '( ps-line-numner 'non-nil ) ;;; Imprimir sempre os numeros das linhas
 '( auto-save-list-file-prefix ( expand-file-name "var/auto-save/" user-emacs-directory ))
 '( backup-by-copying t )
 '( backup-directory-alist
    ( list
      ( cons "."
        ( expand-file-name "var/backup/" user-emacs-directory ))))

 '( calendar-mark-holidays-flag t )
 '( cider-repl-display-help-banner nil )
 '( cider-repl-display-in-current-window nil )
 '( cider-repl-pop-to-buffer-on-connect 'display-only )
 '( cider-xref-fn-depth 90 )
 '( clojure-toplevel-inside-comment-form t )
 '( column-number-mode t )
 '( completion-category-overrides '(( file ( styles basic orderless ))))
 '( completion-styles '( orderless ))
 '( context-menu-mode t )
 '( custom-safe-themes
    '( "14436a10b0cb5b7b6e6f6d490a08c1a751ad0384e9b124b9b8d5d554129f5571" default ))
 '( delete-by-moving-to-trash t )
 '( delete-old-versions t )

 '( echo-keystrokes 0.2 )
 '( ediff-split-window-function #'split-window-horizontally )
 '( ediff-window-setup-function #'ediff-setup-windows-plain )
 '( enable-recursive-minibuffers t )

 '( fill-column 80 )
 '( find-ls-option '( "-print0 | xargs -0 ls -ld" . "-ld" ))
 '( global-goto-address-mode t )
 '( global-so-long-mode t )
 '( grep-find-command '( "rg -n -H --no-heading --glob='' -e ''" . 37 ))
 '( helpful-switch-buffer-function #'helpful-switch-to-buffer )
 '( history-length 20000 )
 '( hscroll-margin 2 )
 '( hscroll-step 1 )
 '( indent-tabs-mode nil )
 '( indicate-empty-lines t )
 '( inhibit-startup-screen t )
 '( initial-major-mode 'fundamental-mode )
 '( initial-scratch-message nil )
 '( isearch-allow-motion t )
 '( isearch-allow-scroll t )
 '( isearch-lazy-count t )
 '( isearch-wrap-pause 'no )
 '( isearch-yank-on-move t )
 '( kept-new-versions 6 )
 '( kept-old-versions 2 )
 '( kill-do-not-save-duplicates t )
 '( lazy-count-prefix-format nil )
 '( lazy-count-suffix-format " [%s/%s]" )
 '( line-spacing 3 )

 '( make-backup-files t )
 '( mode-line-compact 'long )
 '( mouse-wheel-flip-direction t )
 '( mouse-wheel-scroll-amount
    '( 2
       (( shift )
        . hscroll )
       (( meta ))
       (( control meta )
        . global-text-scale )
       (( control )
        . text-scale )))
 '( mouse-wheel-scroll-amount-horizontal 2 )
 '( mouse-wheel-tilt-scroll t )
 '( next-error-message-highlight t )


 '( pixel-scroll-precision-mode t )
 '( recentf-max-saved-items 200 )
 '( recentf-mode t )
 '( repeat-mode t )
 '( ring-bell-function 'flash-mode-line )
 '( save-place-mode t )
 '( savehist-additional-variables
    '( kill-ring mark-ring global-mark-ring search-ring regexp-search-ring ))
 '( savehist-mode t )
 '( savehist-save-minibuffer-history t )
 '( scroll-conservatively 101 )
 '( scroll-preserve-screen-position t )
 '( search-whitespace-regexp ".*?" )
 '( show-paren-context-when-offscreen t )
 '( show-paren-mode t )
 '( shr-cookie-policy nil )
 '( shr-discard-aria-hidden t )
 '( shr-image-animate nil )
 '( shr-indentation 0 t )
 '( shr-max-image-proportion 0.5 )
 '( shr-use-colors nil )
 '( shr-use-fonts nil )
 '( shr-width 72 )
 '( sqlformat-args '( "-s2" "-g" ))
 '( sqlformat-command 'pgformatter )
 '( tab-always-indent 'complete )
 '( tab-width 4 )
 '( tramp-default-method "ssh" )
 '( truncate-lines t )
 '( truncate-partial-width-windows nil )
 '( uniquify-buffer-name-style 'forward nil ( uniquify ))
 '( version-control t )
 '( visible-bell nil )
 '( word-wrap t )
 '( ws-butler-keep-whitespace-before-point nil )
 '( x-stretch-cursor t )
 '( xref-search-program 'ripgrep ))



( setq-default
  frame-title-format '( "%n "              ;;; narrowed?
                        ( :eval
                          ( if ( buffer-file-name )
                              ( abbreviate-file-name (buffer-file-name ))
                            "%b" )))
  mode-line-format
  ( remove '(vc-mode vc-mode ) mode-line-format ))
( defun flash-mode-line ( )
  "Flash the modeline on error or warning.
https://macowners.club/posts/custom-functions-4-ui/"
  ( invert-face 'mode-line )
  ( run-with-timer 0.1 nil #'invert-face 'mode-line ))

( put 'narrow-to-region 'disabled nil )
( put 'dired-find-alternate-file 'disabled nil )
( put 'downcase-region 'disabled nil )
( put 'upcase-region 'disabled nil )
( put 'set-goal-column 'disabled nil )



( provide 'init.el )
;;; init.el ends here
