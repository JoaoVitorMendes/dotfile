# Operator-caska Cursive Font
Instalando fonts para Emacs 



## Installation
Clone this repo:
```sh
git clone https://github.com/Anant-mishra1729/Operator-caska-Font.git
cd Operator-caska-Font
```

### Linux

* Faca uma copia para o diretorio das fontes
```sh
cp -r *.ttf ~/.local/share/fonts/
```

* atualize a font dos cache
```sh
fc-cache -f -v
```


### Issues
* **Operator-caskabold in Windows**: VScode font is not cursive (Issue only with Windows)

You'll have to **[modify the textmate rules](https://code.visualstudio.com/api/language-extensions/syntax-highlight-guide)**, here's an example, paste this in **settings.json**

```js
"editor.tokenColorCustomizations": {
    "textMateRules": [
      {
        "scope": [
          "comment",
          "entity.name.type.class", // class names
          "keyword", // import, export, return
          "constant", // String, Number, Boolean..., this, super
          "storage.type",
          "variable.language.this.cpp"
        ],
        "settings": {
          "fontStyle": "italic bold" // comments are italic
        }
      },
    ]
  },
```

**Output**
|Without textmate rules|With textmate rules|
|-|-|
|![image](https://github.com/Anant-mishra1729/Operator-caska-Font/assets/84588156/ab06dbfd-9218-4544-9690-eb3e48ee4633)|![image](https://github.com/Anant-mishra1729/Operator-caska-Font/assets/84588156/e7d69e5a-a31f-4b27-b967-39dc1ba3a44d)|
